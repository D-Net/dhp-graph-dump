
package eu.dnetlib.dhp.oa.zenodoapi.model;

/**
 * @author miriam.baglioni
 * @Date 01/07/23
 */
/**
 * @author miriam.baglioni
 * @Date 01/07/23
 */
import java.io.Serializable;

public class PrereserveDoi implements Serializable {
	private String doi;
	private String recid;

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getRecid() {
		return recid;
	}

	public void setRecid(String recid) {
		this.recid = recid;
	}
}
