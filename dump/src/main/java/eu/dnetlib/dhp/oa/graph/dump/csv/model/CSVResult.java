
package eu.dnetlib.dhp.oa.graph.dump.csv.model;

import java.io.Serializable;

import eu.dnetlib.dhp.oa.graph.dump.csv.Constants;

/**
 * @author miriam.baglioni
 * @Date 11/05/23
 */
public class CSVResult implements Serializable {
	private String id;
	private String type;
	private String title;
	private String description;
	private String accessright;
	private String publication_date;
	private String publisher;
	private String keywords;
	private String country;
	private String language;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = Constants.addQuotes(id);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = Constants.addQuotes(type);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = Constants.addQuotes(title);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = Constants.addQuotes(description);
	}

	public String getAccessright() {
		return accessright;
	}

	public void setAccessright(String accessright) {
		this.accessright = Constants.addQuotes(accessright);
	}

	public String getPublication_date() {
		return publication_date;
	}

	public void setPublication_date(String publication_date) {
		this.publication_date = Constants.addQuotes(publication_date);
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = Constants.addQuotes(publisher);
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = Constants.addQuotes(keywords);
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = Constants.addQuotes(country);
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = Constants.addQuotes(language);
	}

}
