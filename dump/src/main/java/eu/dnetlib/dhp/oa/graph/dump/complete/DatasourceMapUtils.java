
package eu.dnetlib.dhp.oa.graph.dump.complete;

import static eu.dnetlib.dhp.oa.graph.dump.Utils.*;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.mapFieldString;

import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.Container;
import eu.dnetlib.dhp.oa.model.graph.Datasource;
import eu.dnetlib.dhp.oa.model.graph.DatasourcePid;
import eu.dnetlib.dhp.oa.model.graph.DatasourceSchemeValue;
import eu.dnetlib.dhp.schema.oaf.*;

public class DatasourceMapUtils {
	public static Datasource mapDatasource(eu.dnetlib.dhp.schema.oaf.Datasource d) {
		if (Boolean.TRUE.equals(d.getDataInfo().getDeletedbyinference()))
			return null;
		Datasource datasource = new Datasource();

		datasource.setId(getEntityId(d.getId(), ENTITY_ID_SEPARATOR));
		datasource.setOriginalIds(mapListString(d.getOriginalId(), v -> v));
		datasource
			.setPids(mapPid(d.getPid(), p -> DatasourcePid.newInstance(p.getQualifier().getClassid(), p.getValue())));
		datasource.setType(mapSchemaValue(d.getDatasourcetype()));
		datasource.setOpenaireCompatibility(mapQualifierName(d.getOpenairecompatibility()));
		datasource.setOfficialName(mapFieldString(d.getOfficialname()));
		datasource.setEnglishName(mapFieldString(d.getEnglishname()));
		datasource.setWebsiteUrl(mapFieldString(d.getWebsiteurl()));
		datasource.setLogoUrl(mapFieldString(d.getLogourl()));
		datasource.setDateOfValidation(mapFieldString(d.getDateofvalidation()));
		datasource.setDescription(mapFieldString(d.getDescription()));
		datasource.setSubjects(Utils.mapListString(d.getSubjects(), StructuredProperty::getValue));
		datasource.setPolicies(Utils.mapListString(d.getPolicies(), KeyValue::getKey));
		datasource.setLanguages(Utils.mapListString(d.getOdlanguages(), Field::getValue));
		datasource.setContentTypes(Utils.mapListString(d.getOdcontenttypes(), Field::getValue));
		datasource.setReleaseStartDate(mapFieldString(d.getReleaseenddate()));
		datasource.setReleaseEndDate(mapFieldString(d.getReleaseenddate()));
		datasource.setMissionStatementUrl(mapFieldString(d.getMissionstatementurl()));
		datasource.setAccessRights(mapFieldString(d.getDatabaseaccesstype()));
		datasource.setUploadRights(mapFieldString(d.getDatauploadtype()));
		datasource.setDatabaseAccessRestriction(mapFieldString(d.getDatabaseaccessrestriction()));
		datasource.setDataUploadRestriction(mapFieldString(d.getDatauploadrestriction()));
		datasource.setVersioning(mapFieldBool(d.getVersioning()));
		datasource.setCitationGuidelineUrl(mapFieldString(d.getCitationguidelineurl()));
		datasource.setPidSystems(mapFieldString(d.getPidsystems()));
		datasource.setCertificates(mapFieldString(d.getCertificates()));
		datasource.setJournal(mapContainer(d.getJournal()));

		return datasource;

	}

	private static DatasourceSchemeValue mapSchemaValue(Qualifier datasourcetype) {
		if (datasourcetype == null)
			return null;
		return DatasourceSchemeValue.newInstance(datasourcetype.getClassid(), datasourcetype.getClassname());
	}

//	private static Container getContainer(Journal j) {
//		Container c = new Container();
//
//		c.setName(j.getName());
//		c.setIssnPrinted(j.getIssnPrinted());
//		c.setIssnOnline(j.getIssnOnline());
//		c.setIssnLinking(j.getIssnLinking());
//		c.setEp(j.getEp());
//		c.setIss(j.getIss());
//		c.setSp(j.getSp());
//		c.setVol(j.getVol());
//		c.setEdition(j.getEdition());
//		c.setConferenceDate(j.getConferencedate());
//		c.setConferencePlace(j.getConferenceplace());
//		return c;
//	}
}
