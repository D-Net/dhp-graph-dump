
package eu.dnetlib.dhp.oa.graph.dump.csv.model;

import java.io.Serializable;

import eu.dnetlib.dhp.oa.graph.dump.csv.Constants;

/**
 * @author miriam.baglioni
 * @Date 11/05/23
 */
public class CSVRelResAut implements Serializable {
	private String result_id;
	private String author_id;

	public String getResult_id() {
		return result_id;
	}

	public void setResult_id(String result_id) {
		this.result_id = Constants.addQuotes(result_id);
	}

	public String getAuthor_id() {
		return author_id;
	}

	public void setAuthor_id(String author_id) {
		this.author_id = Constants.addQuotes(author_id);
	}
}
