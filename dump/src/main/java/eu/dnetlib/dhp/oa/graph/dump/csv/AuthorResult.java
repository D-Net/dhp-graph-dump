
package eu.dnetlib.dhp.oa.graph.dump.csv;

import java.io.Serializable;

import eu.dnetlib.dhp.utils.DHPUtils;

/**
 * @author miriam.baglioni
 * @Date 05/05/23
 */
public class AuthorResult implements Serializable {
	private String authorId;
	private String firstName;
	private String lastName;
	private String fullName;
	private String orcid;
	private String resultId;
	private String rank;
	private Boolean fromOrcid;

	public Boolean getFromOrcid() {
		return fromOrcid;
	}

	public void setFromOrcid(Boolean fromOrcid) {
		this.fromOrcid = fromOrcid;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAuthorId() {
		return authorId;
	}

	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}

	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getId() {
		return authorId;
	}

	public void setId(String id) {
		this.authorId = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOrcid() {
		return orcid;
	}

	public void setOrcid(String orcid) {
		this.orcid = orcid;
	}

	public void autosetId() {
		if (orcid != null) {
			authorId = DHPUtils.md5(orcid);
		} else {
			authorId = DHPUtils.md5(resultId + rank);
		}

	}
}
