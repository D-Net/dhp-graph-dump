
package eu.dnetlib.dhp.oa.graph.dump.subset.selectionconstraints;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author miriam.baglioni
 * @Date 11/11/22
 */
public class ProtoMap extends HashMap<String, String> implements Serializable {
	public ProtoMap() {
		super();
	}
}
