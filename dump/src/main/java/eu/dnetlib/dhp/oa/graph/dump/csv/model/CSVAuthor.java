
package eu.dnetlib.dhp.oa.graph.dump.csv.model;

import java.io.Serializable;

import eu.dnetlib.dhp.oa.graph.dump.csv.Constants;

/**
 * @author miriam.baglioni
 * @Date 11/05/23
 */
public class CSVAuthor implements Serializable {
	private String id;
	private String firstname;
	private String lastname;
	private String fullname;
	private String orcid;
	private Boolean fromOrcid;

	public Boolean getFromOrcid() {
		return fromOrcid;
	}

	public void setFromOrcid(Boolean fromOrcid) {
		this.fromOrcid = fromOrcid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = Constants.addQuotes(id);
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = Constants.addQuotes(firstname);
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = Constants.addQuotes(lastname);
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = Constants.addQuotes(fullname);
	}

	public String getOrcid() {
		return orcid;
	}

	public void setOrcid(String orcid) {
		this.orcid = Constants.addQuotes(orcid);
	}

}
