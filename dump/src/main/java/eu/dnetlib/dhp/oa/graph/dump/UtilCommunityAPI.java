
package eu.dnetlib.dhp.oa.graph.dump;

import static eu.dnetlib.dhp.utils.DHPUtils.MAPPER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.cxf.common.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.communityapi.model.*;
import eu.dnetlib.dhp.oa.graph.dump.community.CommunityMap;
import eu.dnetlib.dhp.oa.graph.dump.complete.ContextInfo;
import eu.dnetlib.dhp.oa.graph.dump.csv.Constants;
import eu.dnetlib.dhp.utils.DHPUtils;

public class UtilCommunityAPI {

	private static final Logger log = LoggerFactory.getLogger(UtilCommunityAPI.class);

	public CommunityMap getCommunityMap(boolean singleCommunity, String communityId)
		throws IOException {
		if (singleCommunity)
			return getMap(Arrays.asList(getCommunity(communityId)));
		return getMap(getValidCommunities());

	}

	private CommunityMap getMap(List<CommunityModel> communities) {
		final CommunityMap map = new CommunityMap();
		communities.forEach(c -> map.put(c.getId(), c.getName()));
		return map;
	}

	public List<String> getCommunityCsv(List<String> comms) {
		return comms.stream().map(c -> {
			try {
				CommunityModel community = getCommunity(c);
				StringBuilder builder = new StringBuilder();
				builder.append(DHPUtils.md5(community.getId()));
				builder.append(Constants.SEP);
				builder.append(community.getName());
				builder.append(Constants.SEP);
				builder.append(community.getId());
				builder.append(Constants.SEP);
				builder
					.append(
						community.getDescription());
				return builder.toString();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}).collect(Collectors.toList());

	}

	private List<CommunityModel> getValidCommunities() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper
			.readValue(eu.dnetlib.dhp.communityapi.QueryCommunityAPI.communities(), CommunitySummary.class)
			.stream()
			.filter(
				community -> (community.getStatus().equals("all") || community.getStatus().equalsIgnoreCase("public"))
					&&
					(community.getType().equals("ri") || community.getType().equals("community")))
			.collect(Collectors.toList());

	}

	private CommunityModel getCommunity(String id) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper
			.readValue(eu.dnetlib.dhp.communityapi.QueryCommunityAPI.community(id), CommunityModel.class);

	}

	public List<ContextInfo> getContextInformation() throws IOException {

		return getValidCommunities()
			.stream()
			.map(c -> getContext(c))
			.collect(Collectors.toList());

	}

	public ContextInfo getContext(CommunityModel c) {

		ContextInfo cinfo = new ContextInfo();
		cinfo.setName(c.getName());
		cinfo.setId(c.getId());
		cinfo.setDescription(c.getDescription());
		CommunityModel cm = null;
		try {
			cm = getCommunity(c.getId());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		if (!CollectionUtils.isEmpty(cm.getSubjects())) {
			cinfo.setSubject(new ArrayList<>());
			cinfo.getSubject().addAll(cm.getSubjects());
		}
		cinfo.setZenodocommunity(c.getZenodoCommunity());
		cinfo.setType(c.getType());
		return cinfo;
	}

	public List<ContextInfo> getContextRelation() throws IOException {
		return getValidCommunities().stream().map(c -> {
			ContextInfo cinfo = new ContextInfo();
			cinfo.setId(c.getId());
			cinfo.setDatasourceList(getDatasourceList(c.getId()));
			cinfo.setProjectList(getProjectList(c.getId()));

			return cinfo;
		}).collect(Collectors.toList());
	}

	private List<String> getDatasourceList(String id) {
		List<String> datasourceList = new ArrayList<>();
		try {

			new ObjectMapper()
				.readValue(
					eu.dnetlib.dhp.communityapi.QueryCommunityAPI.communityDatasource(id),
					DatasourceList.class)
				.stream()
				.forEach(ds -> {
					if (Optional.ofNullable(ds.getOpenaireId()).isPresent()) {

						datasourceList.add(ds.getOpenaireId());
					}

				});

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return datasourceList;
	}

	private List<String> getProjectList(String id) {
		int page = -1;
		int size = 100;
		ContentModel cm = null;
		;
		ArrayList<String> projectList = new ArrayList<>();
		do {
			page++;
			try {
				cm = new ObjectMapper()
					.readValue(
						eu.dnetlib.dhp.communityapi.QueryCommunityAPI
							.communityProjects(
								id, String.valueOf(page), String.valueOf(size)),
						ContentModel.class);
				if (cm.getContent().size() > 0) {
					cm.getContent().forEach(p -> {
						if (Optional.ofNullable(p.getOpenaireId()).isPresent())
							projectList.add(p.getOpenaireId());

					});
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} while (!cm.getLast());

		return projectList;
	}

	/**
	 * it returns for each organization the list of associated communities
	 */
	public CommunityEntityMap getCommunityOrganization() throws IOException {
		CommunityEntityMap organizationMap = new CommunityEntityMap();
		getValidCommunities()
			.forEach(community -> {
				String id = community.getId();
				try {
					List<String> associatedOrgs = MAPPER
						.readValue(
							eu.dnetlib.dhp.communityapi.QueryCommunityAPI.communityPropagationOrganization(id),
							OrganizationList.class);
					associatedOrgs.forEach(o -> {
						if (!organizationMap
							.keySet()
							.contains(o))
							organizationMap.put(o, new ArrayList<>());
						organizationMap.get(o).add(community.getId());
					});
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			});

		return organizationMap;
	}

}
