
package eu.dnetlib.dhp.oa.graph.dump.subset.criteria;

public class VerbResolverFactory {

	private VerbResolverFactory() {
	}

	public static VerbResolver newInstance() {

		return new VerbResolver();
	}
}
