
package eu.dnetlib.dhp.oa.graph.dump.subset.criteria;

import java.io.Serializable;

public interface Selection extends Serializable {

	boolean apply(String value);
}
