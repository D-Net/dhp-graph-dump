
package eu.dnetlib.dhp.oa.graph.dump;

import java.util.Map;

import com.google.common.collect.Maps;

import eu.dnetlib.dhp.schema.common.ModelConstants;

public class Constants {

	protected static final Map<String, String> ACCESS_RIGHTS_COAR_MAP = Maps.newHashMap();
	protected static final Map<String, String> COAR_CODE_LABEL_MAP = Maps.newHashMap();

	public static final String INFERRED = "Inferred by OpenAIRE";
	public static final String CABF2 = "c_abf2";

	public static final String HARVESTED = "Harvested";
	public static final String DEFAULT_TRUST = "0.9";
	public static final String USER_CLAIM = "Linked by user";

	public static final String COAR_ACCESS_RIGHT_SCHEMA = "http://vocabularies.coar-repositories.org/documentation/access_rights/";

	public static final String ZENODO_COMMUNITY_PREFIX = "https://zenodo.org/communities/";

	public static final String RESEARCH_COMMUNITY = "Research Community";

	public static final String RESEARCH_INFRASTRUCTURE = "Research Infrastructure";

	public static final String USAGE_COUNT_DOWNLOADS = "downloads";
	public static final String USAGE_COUNT_VIEWS = "views";
	public static final String BIP_POPULARITY = "popularity";
	public static final String BIP_POPULARITY_ALT = "popularity_alt";
	public static final String BIP_INFLUENCE = "influence";
	public static final String BIP_INFLUENCE_ALT = "influence_alt";
	public static final String BIP_IMPULSE = "impulse";
	public static final String BIP_CITATION_COUNT = "citation";

	static {
		ACCESS_RIGHTS_COAR_MAP.put(ModelConstants.ACCESS_RIGHT_OPEN, CABF2);
		ACCESS_RIGHTS_COAR_MAP.put("RESTRICTED", "c_16ec");
		ACCESS_RIGHTS_COAR_MAP.put("OPEN SOURCE", CABF2);
		ACCESS_RIGHTS_COAR_MAP.put(ModelConstants.ACCESS_RIGHT_CLOSED, "c_14cb");
		ACCESS_RIGHTS_COAR_MAP.put(ModelConstants.ACCESS_RIGHT_EMBARGO, "c_f1cf");
	}

	static {
		COAR_CODE_LABEL_MAP.put(CABF2, ModelConstants.ACCESS_RIGHT_OPEN);
		COAR_CODE_LABEL_MAP.put("c_16ec", "RESTRICTED");
		COAR_CODE_LABEL_MAP.put("c_14cb", ModelConstants.ACCESS_RIGHT_CLOSED);
		COAR_CODE_LABEL_MAP.put("c_f1cf", "EMBARGO");
	}

	public enum DUMPTYPE {
		COMPLETE("complete"), COMMUNITY("community"), FUNDER("funder");

		private final String type;

		DUMPTYPE(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}
}
