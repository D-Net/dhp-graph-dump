
package eu.dnetlib.dhp.oa.graph.dump.csv.model;

import java.io.Serializable;

import eu.dnetlib.dhp.oa.graph.dump.csv.Constants;

/**
 * @author miriam.baglioni
 * @Date 11/05/23
 */
public class CSVCitation implements Serializable {
	private String id;
	private String result_id_cites;
	private String result_id_cited;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = Constants.addQuotes(id);
	}

	public String getResult_id_cites() {
		return result_id_cites;
	}

	public void setResult_id_cites(String result_id_cites) {
		this.result_id_cites = Constants.addQuotes(result_id_cites);
	}

	public String getResult_id_cited() {
		return result_id_cited;
	}

	public void setResult_id_cited(String result_id_cited) {
		this.result_id_cited = Constants.addQuotes(result_id_cited);
	}
}
