
package eu.dnetlib.dhp.oa.graph.dump.complete;

import static eu.dnetlib.dhp.oa.graph.dump.Utils.*;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.mapFieldString;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.graph.*;
import eu.dnetlib.dhp.schema.oaf.Field;
import eu.dnetlib.dhp.schema.oaf.H2020Classification;
import eu.dnetlib.dhp.schema.oaf.StructuredProperty;

public class ProjectMapUtils {

	public static Project mapProject(eu.dnetlib.dhp.schema.oaf.Project p) throws DocumentException {
		if (Boolean.TRUE.equals(p.getDataInfo().getDeletedbyinference()))
			return null;

		Project project = new Project();

		project.setId(getEntityId(p.getId(), ENTITY_ID_SEPARATOR));
		project.setWebsiteUrl(mapFieldString(p.getWebsiteurl()));
		project.setCode(mapFieldString(p.getCode()));
		project.setAcronym(mapFieldString(p.getAcronym()));
		project.setTitle(mapFieldString(p.getTitle()));
		project.setStartDate(mapFieldString(p.getStartdate()));
		project.setEndDate(mapFieldString(p.getEnddate()));
		project.setCallIdentifier(mapFieldString(p.getCallidentifier()));
		project.setKeywords(mapFieldString(p.getKeywords()));
		project
			.setOpenAccessMandateForPublications(
				mapStringAsBoolean(p.getOamandatepublications()) || mapStringAsBoolean(p.getEcarticle29_3()));
		project.setOpenAccessMandateForDataset(mapStringAsBoolean(p.getEcarticle29_3()));
		project.setSubjects(Utils.mapListString(p.getSubjects(), StructuredProperty::getValue));
		project.setSummary(mapFieldString(p.getSummary()));
		project.setGranted(mapGranted(p.getFundedamount(), p.getCurrency(), p.getTotalcost()));
		project.setH2020Programmes(mapH2020Programmes(p.getH2020classification()));
		project.setFundings(mapFundings(p.getFundingtree()));
		return project;
	}

	private static List<Funder> mapFundings(List<Field<String>> fundingtrees) throws DocumentException {
		if (fundingtrees == null || fundingtrees.isEmpty())
			return null;
		List<Funder> funList = new ArrayList<>();

		for (Field<String> fundingtree : fundingtrees)
			funList.add(getFunder(fundingtree.getValue()));
		return funList;
	}

	private static List<Programme> mapH2020Programmes(List<H2020Classification> h2020classification) {
		if (h2020classification == null || h2020classification.isEmpty())
			return null;

		return h2020classification
			.stream()
			.map(
				c -> Programme
					.newInstance(
						c.getH2020Programme().getCode(), c.getH2020Programme().getDescription()))
			.collect(Collectors.toList());

	}

	private static Granted mapGranted(Float fundedamount, Field<String> currency, Float totalcost) {
		if (currency == null || currency.getValue() == null || currency.getValue().isEmpty())
			return null;
		if (fundedamount == null)
			return null;
		if (totalcost == null)
			return Granted.newInstance(currency.getValue(), fundedamount);
		else
			return Granted.newInstance(currency.getValue(), totalcost, fundedamount);
	}

	private static Boolean mapStringAsBoolean(Field<String> oamandate) {

		if (oamandate == null || oamandate.getValue() == null || oamandate.getValue().trim().isEmpty())
			return null;
		return Boolean.valueOf(oamandate.getValue());

	}

	public static Funder getFunder(String fundingtree) throws DocumentException {
		Funder f = new Funder();
		final Document doc;

		doc = new SAXReader().read(new StringReader(fundingtree));
		f.setShortName(((org.dom4j.Node) (doc.selectNodes("//funder/shortname").get(0))).getText());
		f.setName(((org.dom4j.Node) (doc.selectNodes("//funder/name").get(0))).getText());
		f.setJurisdiction(((org.dom4j.Node) (doc.selectNodes("//funder/jurisdiction").get(0))).getText());

		String id = "";

		StringBuilder bld = new StringBuilder();

		int level = 0;
		List<org.dom4j.Node> nodes = doc.selectNodes("//funding_level_" + level);
		while (!nodes.isEmpty()) {
			for (org.dom4j.Node n : nodes) {

				List node = n.selectNodes("./id");
				id = ((org.dom4j.Node) node.get(0)).getText();
				id = id.substring(id.indexOf("::") + 2);

				node = n.selectNodes("./description");
				bld.append(((Node) node.get(0)).getText() + " - ");

			}
			level += 1;
			nodes = doc.selectNodes("//funding_level_" + level);
		}
		String description = bld.toString();
		if (!id.equals("")) {
			Fundings fundings = new Fundings();
			fundings.setId(id);
			fundings.setDescription(description.substring(0, description.length() - 3).trim());
			f.setFundingStream(fundings);
		}

		return f;

	}
}
