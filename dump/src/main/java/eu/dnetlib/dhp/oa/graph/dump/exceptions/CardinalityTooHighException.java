
package eu.dnetlib.dhp.oa.graph.dump.exceptions;

public class CardinalityTooHighException extends Exception {
	public CardinalityTooHighException() {
		super();
	}

	public CardinalityTooHighException(
		final String message,
		final Throwable cause,
		final boolean enableSuppression,
		final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CardinalityTooHighException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public CardinalityTooHighException(final String message) {
		super(message);
	}

	public CardinalityTooHighException(final Throwable cause) {
		super(cause);
	}

}
