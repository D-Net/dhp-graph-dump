
package eu.dnetlib.dhp.oa.graph.dump.complete;

import static eu.dnetlib.dhp.common.SparkSessionSupport.runWithSparkSession;

import java.io.Serializable;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.application.ArgumentApplicationParser;
import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.dnetlib.dhp.oa.model.graph.Relation;
import it.unimi.dsi.fastutil.objects.Object2BooleanMap;
import scala.Tuple2;

/**
 * Reads all the entities of the same type (Relation / Results) and saves them in the same folder
 */
public class SparkCollectAndSave implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(SparkCollectAndSave.class);

	public static void main(String[] args) throws Exception {
		String jsonConfiguration = IOUtils
			.toString(
				SparkCollectAndSave.class
					.getResourceAsStream(
						"/eu/dnetlib/dhp/oa/graph/dump/input_collect_and_save.json"));

		final ArgumentApplicationParser parser = new ArgumentApplicationParser(jsonConfiguration);
		parser.parseArgument(args);

		Boolean isSparkSessionManaged = Optional
			.ofNullable(parser.get("isSparkSessionManaged"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);
		log.info("isSparkSessionManaged: {}", isSparkSessionManaged);

		final String inputPath = parser.get("sourcePath");
		log.info("inputPath: {}", inputPath);

		final String outputPath = parser.get("outputPath");
		log.info("outputPath: {}", outputPath);

		final Boolean aggregateResult = Optional
			.ofNullable(parser.get("resultAggregation"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);

		SparkConf conf = new SparkConf();

		runWithSparkSession(
			conf,
			isSparkSessionManaged,
			spark -> {
				Utils.removeOutputDir(spark, outputPath + "/result");
				run(spark, inputPath, outputPath, aggregateResult);

			});

	}

	private static void run(SparkSession spark, String inputPath, String outputPath, boolean aggregate) {
		spark.conf().set("spark.sql.json.allowNull", "true");
		if (aggregate) {
			Utils
				.readPath(spark, inputPath + "/result/publication", GraphResult.class)
				.union(Utils.readPath(spark, inputPath + "/result/dataset", GraphResult.class))
				.union(Utils.readPath(spark, inputPath + "/result/otherresearchproduct", GraphResult.class))
				.union(Utils.readPath(spark, inputPath + "/result/software", GraphResult.class))
				.map(
					(MapFunction<GraphResult, String>) r -> new ObjectMapper().writeValueAsString(r), Encoders.STRING())
				.write()
				.option("compression", "gzip")
				.mode(SaveMode.Overwrite)
				.text(outputPath + "/result");
		} else {
			write(
				Utils
					.readPath(spark, inputPath + "/result/publication", GraphResult.class),
				outputPath + "/publication");
			write(
				Utils
					.readPath(spark, inputPath + "/result/dataset", GraphResult.class),
				outputPath + "/dataset");
			write(
				Utils
					.readPath(spark, inputPath + "/result/otherresearchproduct", GraphResult.class),
				outputPath + "/otherresearchproduct");
			write(
				Utils
					.readPath(spark, inputPath + "/result/software", GraphResult.class),
				outputPath + "/software");

		}

		// Dataset<String> dumpedIds = Utils.getEntitiesId(spark, outputPath);

//		Dataset<Relation> relations = Utils
//			.readPath(spark, inputPath + "/relation/publication", Relation.class)
//			.union(Utils.readPath(spark, inputPath + "/relation/dataset", Relation.class))
//			.union(Utils.readPath(spark, inputPath + "/relation/orp", Relation.class))
//			.union(Utils.readPath(spark, inputPath + "/relation/software", Relation.class))
//			.union(Utils.readPath(spark, inputPath + "/relation/contextOrg", Relation.class))
//			.union(Utils.readPath(spark, inputPath + "/relation/context", Relation.class))
//			.union(Utils.readPath(spark, inputPath + "/relation/relation", Relation.class));

//		Utils.getValidRelations(relations, Utils.getEntitiesId(spark, outputPath))
		Utils
			.readPath(spark, inputPath + "/relation/publication", Relation.class)
			.write()
			.mode(SaveMode.Overwrite)
			.option("compression", "gzip")
			.json(outputPath + "/relation");
		Utils
			.readPath(spark, inputPath + "/relation/dataset", Relation.class)
			.write()
			.mode(SaveMode.Append)
			.option("compression", "gzip")
			.json(outputPath + "/relation");
		Utils
			.readPath(spark, inputPath + "/relation/orp", Relation.class)
			.write()
			.mode(SaveMode.Append)
			.option("compression", "gzip")
			.json(outputPath + "/relation");

		Utils
			.readPath(spark, inputPath + "/relation/software", Relation.class)
			.write()
			.mode(SaveMode.Append)
			.option("compression", "gzip")
			.json(outputPath + "/relation");
		Utils
			.readPath(spark, inputPath + "/relation/contextOrg", Relation.class)
			.write()
			.mode(SaveMode.Append)
			.option("compression", "gzip")
			.json(outputPath + "/relation");
		Utils
			.readPath(spark, inputPath + "/relation/context", Relation.class)
			.write()
			.mode(SaveMode.Append)
			.option("compression", "gzip")
			.json(outputPath + "/relation");
		Utils
			.readPath(spark, inputPath + "/relation/relation", Relation.class)
			.write()
			.mode(SaveMode.Append)
			.option("compression", "gzip")
			.json(outputPath + "/relation");
//		relSource(
//			inputPath, dumpedIds, Utils
//				.readPath(spark, inputPath + "/relation/publication", Relation.class),
//			inputPath + "/relSource/publication");
//		relSource(
//			inputPath, dumpedIds, Utils
//				.readPath(spark, inputPath + "/relation/dataset", Relation.class),
//			inputPath + "/relSource/dataset");
//		relSource(
//			inputPath, dumpedIds, Utils
//				.readPath(spark, inputPath + "/relation/orp", Relation.class),
//			inputPath + "/relSource/orp");
//		relSource(
//			inputPath, dumpedIds, Utils
//				.readPath(spark, inputPath + "/relation/software", Relation.class),
//			inputPath + "/relSource/software");
//		relSource(
//			inputPath, dumpedIds, Utils
//				.readPath(spark, inputPath + "/relation/contextOrg", Relation.class),
//			inputPath + "/relSource/contextOrg");
//		relSource(
//			inputPath, dumpedIds, Utils
//				.readPath(spark, inputPath + "/relation/context", Relation.class),
//			inputPath + "/relSource/context");
//		relSource(
//			inputPath, dumpedIds, Utils
//				.readPath(spark, inputPath + "/relation/relation", Relation.class),
//			inputPath + "/relSource/relation");

//		relTarget(
//			outputPath, dumpedIds, Utils.readPath(spark, inputPath + "/relSource/publication", Relation.class),
//			SaveMode.Overwrite);
//		relTarget(
//			outputPath, dumpedIds, Utils.readPath(spark, inputPath + "/relSource/dataset", Relation.class),
//			SaveMode.Append);
//		relTarget(
//			outputPath, dumpedIds, Utils.readPath(spark, inputPath + "/relSource/orp", Relation.class),
//			SaveMode.Append);
//		relTarget(
//			outputPath, dumpedIds, Utils.readPath(spark, inputPath + "/relSource/software", Relation.class),
//			SaveMode.Append);
//		relTarget(
//			outputPath, dumpedIds, Utils.readPath(spark, inputPath + "/relSource/contextOrg", Relation.class),
//			SaveMode.Append);
//		relTarget(
//			outputPath, dumpedIds, Utils.readPath(spark, inputPath + "/relSource/context", Relation.class),
//			SaveMode.Append);
//		relTarget(
//			outputPath, dumpedIds, Utils.readPath(spark, inputPath + "/relSource/relation", Relation.class),
//			SaveMode.Append);

	}

	private static void relTarget(String outputPath, Dataset<String> dumpedIds, Dataset<Relation> relJoinSource,
		SaveMode saveMode) {
		relJoinSource
			.joinWith(dumpedIds, relJoinSource.col("target").equalTo(dumpedIds.col("value")))
			.map(
				(MapFunction<Tuple2<Relation, String>, Relation>) t2 -> t2._1(),
				Encoders.bean(Relation.class))
			.write()
			.mode(saveMode)
			.option("compression", "gzip")
			.json(outputPath + "/relation");
	}

	private static void relSource(String inputPath, Dataset<String> dumpedIds, Dataset<Relation> relations,
		String outputPath) {

		relations
			.joinWith(dumpedIds, relations.col("source").equalTo(dumpedIds.col("value")))
			.map(
				(MapFunction<Tuple2<Relation, String>, Relation>) t2 -> t2._1(),
				Encoders.bean(Relation.class))
			.write()
			.mode(SaveMode.Overwrite)
			.option("compression", "gzip")
			.json(outputPath);
	}

	private static void write(Dataset<GraphResult> dataSet, String outputPath) {
		dataSet
			.map((MapFunction<GraphResult, String>) r -> new ObjectMapper().writeValueAsString(r), Encoders.STRING())
			.write()
			.option("compression", "gzip")
			.mode(SaveMode.Overwrite)
			.text(outputPath);
	}

}
