
package eu.dnetlib.dhp.oa.graph.dump.community;

import static eu.dnetlib.dhp.common.SparkSessionSupport.runWithSparkSession;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.ENTITY_ID_SEPARATOR;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.getEntityId;

import java.io.Serializable;
import java.util.*;

import org.apache.commons.io.IOUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.MapGroupsFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.dnetlib.dhp.application.ArgumentApplicationParser;
import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.OrganizationPid;
import eu.dnetlib.dhp.oa.model.community.Affiliation;
import eu.dnetlib.dhp.oa.model.community.CommunityResult;
import eu.dnetlib.dhp.schema.common.ModelConstants;
import eu.dnetlib.dhp.schema.common.ModelSupport;
import eu.dnetlib.dhp.schema.oaf.Organization;
import eu.dnetlib.dhp.schema.oaf.Relation;
import scala.Tuple2;

/**
 * @author miriam.baglioni
 * @Date 27/07/22
 */
public class SparkExtendResultWithOrganization implements Serializable {
	private static final Logger log = LoggerFactory.getLogger(SparkExtendResultWithOrganization.class);

	public static void main(String[] args) throws Exception {
		String jsonConfiguration = IOUtils
			.toString(
				SparkExtendResultWithOrganization.class
					.getResourceAsStream(
						"/eu/dnetlib/dhp/oa/graph/dump/extend_result_with_organization_parameters.json"));

		final ArgumentApplicationParser parser = new ArgumentApplicationParser(jsonConfiguration);
		parser.parseArgument(args);

		Boolean isSparkSessionManaged = Optional
			.ofNullable(parser.get("isSparkSessionManaged"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);
		log.info("isSparkSessionManaged: {}", isSparkSessionManaged);

		final String inputPath = parser.get("sourcePath");
		log.info("inputPath: {}", inputPath);

		final String workingPath = parser.get("workingPath");
		log.info("workingPath: {}", workingPath);

		SparkConf conf = new SparkConf();

		runWithSparkSession(
			conf,
			isSparkSessionManaged,
			spark -> {
				Utils.removeOutputDir(spark, workingPath + "/affiliation");
				addOrganizations(spark, inputPath, workingPath);
			});
	}

	private static void addOrganizations(SparkSession spark, String inputPath, String workingPath) {

		Dataset<Relation> relations = Utils
			.readPath(spark, inputPath + "/relation", Relation.class)
			.filter(
				(FilterFunction<Relation>) r -> !r.getDataInfo().getDeletedbyinference() &&
					!r.getDataInfo().getInvisible()
					&& r.getSubRelType().equalsIgnoreCase(ModelConstants.AFFILIATION));
		Dataset<Organization> organizations = Utils
			.readPath(spark, inputPath + "/organization", Organization.class);
		relations
			.joinWith(organizations, relations.col("source").equalTo(organizations.col("id")), "left")
			.map((MapFunction<Tuple2<Relation, Organization>, ResultOrganizations>) t2 -> {
				if (t2._2() != null) {
					ResultOrganizations rOrg = new ResultOrganizations();
					rOrg.setResultId(getEntityId(t2._1().getTarget(), ENTITY_ID_SEPARATOR));
					Affiliation org = new Affiliation();
					org.setId(getEntityId(t2._2().getId(), ENTITY_ID_SEPARATOR));
					if (Optional.ofNullable(t2._2().getLegalname()).isPresent()) {
						org.setLegalName(t2._2().getLegalname().getValue());
					} else {
						org.setLegalName("");
					}
					HashMap<String, Set<String>> organizationPids = new HashMap<>();
					if (Optional.ofNullable(t2._2().getPid()).isPresent())
						t2._2().getPid().forEach(p -> {
							if (!organizationPids.containsKey(p.getQualifier().getClassid()))
								organizationPids.put(p.getQualifier().getClassid(), new HashSet<>());
							organizationPids.get(p.getQualifier().getClassid()).add(p.getValue());
						});
					List<OrganizationPid> pids = new ArrayList<>();
					for (String key : organizationPids.keySet()) {
						for (String value : organizationPids.get(key)) {
							OrganizationPid pid = new OrganizationPid();
							pid.setValue(value);
							pid.setScheme(key);
							pids.add(pid);
						}
					}
					org.setPids(pids);
					rOrg.setAffiliation(org);
					return rOrg;
				}
				return null;

			}, Encoders.bean(ResultOrganizations.class))
			.filter(Objects::nonNull)
			.write()
			.mode(SaveMode.Overwrite)
			.option("compression", "gzip")
			.json(workingPath + "/resultOrganization");
		ModelSupport.entityTypes
			.keySet()
			.forEach(entity -> {
				if (ModelSupport.isResult(entity)) {
					Dataset<CommunityResult> results = Utils
						.readPath(spark, workingPath + "/dump/" + entity, CommunityResult.class);
					Dataset<ResultOrganizations> resultOrganization = Utils
						.readPath(spark, workingPath + "/resultOrganization", ResultOrganizations.class);
					results
						.joinWith(
							resultOrganization, results.col("id").equalTo(resultOrganization.col("resultId")), "left")
						.groupByKey(
							(MapFunction<Tuple2<CommunityResult, ResultOrganizations>, String>) t2 -> t2._1().getId(),
							Encoders.STRING())
						.mapGroups(
							(MapGroupsFunction<String, Tuple2<CommunityResult, ResultOrganizations>, CommunityResult>) (
								s, it) -> {
								Tuple2<CommunityResult, ResultOrganizations> first = it.next();
								if (first._2() == null) {
									return first._1();
								}
								CommunityResult ret = first._1();
								List<Affiliation> affiliation = new ArrayList<>();
								Set<String> alreadyInsertedAffiliations = new HashSet<>();
								affiliation.add(first._2().getAffiliation());
								alreadyInsertedAffiliations.add(first._2().getAffiliation().getId());
								it.forEachRemaining(res -> {
									if (!alreadyInsertedAffiliations.contains(res._2().getAffiliation().getId())) {
										affiliation.add(res._2().getAffiliation());
										alreadyInsertedAffiliations.add(res._2().getAffiliation().getId());
									}

								});

								ret.setOrganizations(affiliation);
								return ret;
							}, Encoders.bean(CommunityResult.class))
						.write()
						.mode(SaveMode.Overwrite)
						.option("compression", "gzip")
						.json(workingPath + "/affiliation/" + entity);

				}
			});
	}
}
