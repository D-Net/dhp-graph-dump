
package eu.dnetlib.dhp.oa.graph.dump.subset;

import static eu.dnetlib.dhp.common.SparkSessionSupport.runWithSparkSession;

import java.io.Serializable;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.application.ArgumentApplicationParser;
import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.dnetlib.dhp.oa.model.graph.Relation;
import eu.dnetlib.dhp.oa.model.graph.ResearchCommunity;
import scala.Tuple2;

/**
 * @author miriam.baglioni
 * @Date 15/11/22
 */
public class SparkSelectValidRelationContext implements Serializable {
	private static final Logger log = LoggerFactory.getLogger(SparkSelectValidRelationContext.class);

	public static void main(String[] args) throws Exception {
		String jsonConfiguration = IOUtils
			.toString(
				SparkSelectValidRelationContext.class
					.getResourceAsStream(
						"/eu/dnetlib/dhp/oa/graph/dump/input_select_valid_relation_context_parameters.json"));

		final ArgumentApplicationParser parser = new ArgumentApplicationParser(jsonConfiguration);
		parser.parseArgument(args);

		Boolean isSparkSessionManaged = Optional
			.ofNullable(parser.get("isSparkSessionManaged"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);
		log.info("isSparkSessionManaged: {}", isSparkSessionManaged);

		// results dumped
		final String inputPath = parser.get("sourcePath");
		log.info("inputPath: {}", inputPath);

		final String contextRelationPath = parser.get("contextRelationPath");
		log.info("contextRelationPath: {}", contextRelationPath);

		SparkConf conf = new SparkConf();

		runWithSparkSession(
			conf,
			isSparkSessionManaged,
			spark -> {
				selectValidRelation(spark, inputPath, contextRelationPath);

			});

	}

	private static void selectValidRelation(SparkSession spark, String inputPath,
		String contextRelationPath) {
		// read the results
		Dataset<String> dumpedIds = Utils
			.readPath(spark, inputPath + "/publication", GraphResult.class)
			.map((MapFunction<GraphResult, String>) r -> r.getId(), Encoders.STRING())
			.union(
				Utils
					.readPath(spark, inputPath + "/dataset", GraphResult.class)
					.map((MapFunction<GraphResult, String>) r -> r.getId(), Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/software", GraphResult.class)
					.map((MapFunction<GraphResult, String>) r -> r.getId(), Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/otherresearchproduct", GraphResult.class)
					.map((MapFunction<GraphResult, String>) r -> r.getId(), Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/organization", eu.dnetlib.dhp.oa.model.graph.Organization.class)
					.map(
						(MapFunction<eu.dnetlib.dhp.oa.model.graph.Organization, String>) o -> o.getId(),
						Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/project", eu.dnetlib.dhp.oa.model.graph.Project.class)
					.map(
						(MapFunction<eu.dnetlib.dhp.oa.model.graph.Project, String>) o -> o.getId(), Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/datasource", eu.dnetlib.dhp.oa.model.graph.Datasource.class)
					.map(
						(MapFunction<eu.dnetlib.dhp.oa.model.graph.Datasource, String>) o -> o.getId(),
						Encoders.STRING()));

		Dataset<Tuple2<String, Relation>> relationSource = Utils
			.readPath(spark, contextRelationPath + "/context", Relation.class)
			.union(Utils.readPath(spark, contextRelationPath + "/contextOrg", Relation.class))
			.map(
				(MapFunction<Relation, Tuple2<String, Relation>>) r -> new Tuple2<>(r.getSource(), r),
				Encoders.tuple(Encoders.STRING(), Encoders.bean(Relation.class)));

		Dataset<ResearchCommunity> allowedContext = Utils
			.readPath(spark, inputPath + "/communities_infrastructures", ResearchCommunity.class);

		Dataset<Tuple2<String, Relation>> relJoinSource = relationSource
			.joinWith(dumpedIds, relationSource.col("_1").equalTo(dumpedIds.col("value")))
			.map(
				(MapFunction<Tuple2<Tuple2<String, Relation>, String>, Tuple2<String, Relation>>) t2 -> new Tuple2<>(
					t2._1()._2().getTarget(), t2._1()._2()),
				Encoders.tuple(Encoders.STRING(), Encoders.bean(Relation.class)));

		relJoinSource
			.joinWith(allowedContext, relJoinSource.col("_1").equalTo(allowedContext.col("id")))
			.map(
				(MapFunction<Tuple2<Tuple2<String, Relation>, ResearchCommunity>, Relation>) t2 -> t2._1()._2(),
				Encoders.bean(eu.dnetlib.dhp.oa.model.graph.Relation.class))
			.write()
			.mode(SaveMode.Append)
			.option("compression", "gzip")
			.json(inputPath + "/relation");

		relJoinSource = relationSource
			.joinWith(allowedContext, relationSource.col("_1").equalTo(allowedContext.col("id")))
			.map(
				(MapFunction<Tuple2<Tuple2<String, Relation>, ResearchCommunity>, Tuple2<String, Relation>>) t2 -> new Tuple2<>(
					t2._1()._2().getTarget(), t2._1()._2()),
				Encoders.tuple(Encoders.STRING(), Encoders.bean(Relation.class)));

		relJoinSource
			.joinWith(dumpedIds, relJoinSource.col("_1").equalTo(dumpedIds.col("value")))
			.map(
				(MapFunction<Tuple2<Tuple2<String, Relation>, String>, Relation>) t2 -> t2._1()._2(),
				Encoders.bean(Relation.class))
			.write()
			.mode(SaveMode.Append)
			.option("compression", "gzip")
			.json(inputPath + "/relation");

	}

}
