
package eu.dnetlib.dhp.oa.zenodoapi;

public class MissingConceptDoiException extends Throwable {
	public MissingConceptDoiException(String message) {
		super(message);
	}
}
