
package eu.dnetlib.dhp.oa.graph.dump.subset;

import static eu.dnetlib.dhp.common.SparkSessionSupport.runWithSparkSession;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.getEntitiesId;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.getValidRelations;

import java.io.Serializable;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.dnetlib.dhp.application.ArgumentApplicationParser;
import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.dnetlib.dhp.oa.model.graph.Relation;
import eu.dnetlib.dhp.oa.model.graph.ResearchCommunity;
import scala.Tuple2;

/**
 * @author miriam.baglioni
 * @Date 15/11/22
 */
public class SparkSelectValidRelation implements Serializable {
	private static final Logger log = LoggerFactory.getLogger(SparkSelectValidRelation.class);

	public static void main(String[] args) throws Exception {
		String jsonConfiguration = IOUtils
			.toString(
				SparkSelectValidRelation.class
					.getResourceAsStream(
						"/eu/dnetlib/dhp/oa/graph/dump/input_select_valid_relation_parameters.json"));

		final ArgumentApplicationParser parser = new ArgumentApplicationParser(jsonConfiguration);
		parser.parseArgument(args);

		Boolean isSparkSessionManaged = Optional
			.ofNullable(parser.get("isSparkSessionManaged"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);
		log.info("isSparkSessionManaged: {}", isSparkSessionManaged);

		// results dumped
		final String inputPath = parser.get("sourcePath");
		log.info("inputPath: {}", inputPath);

		// all relations plus those produced via context and extracted from results
		final String relationPath = parser.get("relationPath");
		log.info("relationPath: {}", relationPath);

		SparkConf conf = new SparkConf();

		runWithSparkSession(
			conf,
			isSparkSessionManaged,
			spark -> {
				selectValidRelation(spark, inputPath, relationPath);

			});

	}

	private static void selectValidRelation(SparkSession spark, String inputPath,
		String relationPath) {
		// read the results

		getValidRelations(
			Utils
				.readPath(spark, relationPath, Relation.class),
			getEntitiesId(spark, inputPath))

//		Dataset<Tuple2<String, Relation>> relJoinSource = relationSource
//			.joinWith(dumpedIds, relationSource.col("_1").equalTo(dumpedIds.col("value")))
//			.map(
//				(MapFunction<Tuple2<Tuple2<String, Relation>, String>, Tuple2<String, Relation>>) t2 -> new Tuple2<>(
//					t2._1()._2().getTarget().getId(), t2._1()._2()),
//				Encoders.tuple(Encoders.STRING(), Encoders.bean(Relation.class)));
//
//		relJoinSource
//			.joinWith(dumpedIds, relJoinSource.col("_1").equalTo(dumpedIds.col("value")))
//			.map(
//				(MapFunction<Tuple2<Tuple2<String, Relation>, String>, Relation>) t2 -> t2._1()._2(),
//				Encoders.bean(Relation.class))
				.write()
				.mode(SaveMode.Overwrite)
				.option("compression", "gzip")
				.json(inputPath + "/relation");

//		relJoinSource = relationSource
//			.joinWith(dumpedIds, relationSource.col("_1").equalTo(dumpedIds.col("value")))
//			.map(
//				(MapFunction<Tuple2<Tuple2<String, Relation>, String>, Tuple2<String, Relation>>) t2 -> new Tuple2<>(
//					t2._1()._2().getTarget().getId(), t2._1()._2()),
//				Encoders.tuple(Encoders.STRING(), Encoders.bean(Relation.class)));
//
//		relJoinSource
//			.joinWith(dumpedIds, relJoinSource.col("_1").equalTo(dumpedIds.col("value")))
//			.map(
//				(MapFunction<Tuple2<Tuple2<String, Relation>, String>, Relation>) t2 -> t2._1()._2(),
//				Encoders.bean(Relation.class))
//			.write()
//			.mode(SaveMode.Append)
//			.option("compression", "gzip")
//			.json(inputPath + "/relation");

	}

}
