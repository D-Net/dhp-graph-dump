
package eu.dnetlib.dhp.oa.graph.dump.organizationonly;

import static eu.dnetlib.dhp.common.SparkSessionSupport.runWithSparkSession;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.ENTITY_ID_SEPARATOR;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.getEntityId;

import java.io.Serializable;
import java.io.StringReader;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.application.ArgumentApplicationParser;
import eu.dnetlib.dhp.oa.graph.dump.Constants;
import eu.dnetlib.dhp.oa.graph.dump.ResultMapper;
import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.graph.dump.community.CommunityMap;
import eu.dnetlib.dhp.oa.graph.dump.complete.SparkOrganizationRelation;
import eu.dnetlib.dhp.oa.graph.dump.exceptions.CardinalityTooHighException;
import eu.dnetlib.dhp.oa.graph.dump.exceptions.NoAvailableEntityTypeException;
import eu.dnetlib.dhp.oa.model.Container;
import eu.dnetlib.dhp.oa.model.OrganizationPid;
import eu.dnetlib.dhp.oa.model.Provenance;
import eu.dnetlib.dhp.oa.model.Result;
import eu.dnetlib.dhp.oa.model.graph.*;
import eu.dnetlib.dhp.oa.model.graph.Datasource;
import eu.dnetlib.dhp.oa.model.graph.Organization;
import eu.dnetlib.dhp.oa.model.graph.Project;
import eu.dnetlib.dhp.schema.common.ModelSupport;
import eu.dnetlib.dhp.schema.oaf.*;
import eu.dnetlib.dhp.schema.oaf.Relation;
import scala.Tuple2;

/**
 * Spark Job that fires the dump for the entites
 */
public class SparkDumpOrganizationJob implements Serializable {
	private static final Logger log = LoggerFactory
		.getLogger(eu.dnetlib.dhp.oa.graph.dump.organizationonly.SparkDumpOrganizationJob.class);
	public static final String COMPRESSION = "compression";
	public static final String GZIP = "gzip";

	public static void main(String[] args) throws Exception {
		String jsonConfiguration = IOUtils
			.toString(
				SparkOrganizationRelation.class
					.getResourceAsStream(
						"/eu/dnetlib/dhp/oa/graph/dump/organization_only_input_parameters.json"));

		final ArgumentApplicationParser parser = new ArgumentApplicationParser(jsonConfiguration);
		parser.parseArgument(args);

		Boolean isSparkSessionManaged = Optional
			.ofNullable(parser.get("isSparkSessionManaged"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);
		log.info("isSparkSessionManaged: {}", isSparkSessionManaged);

		final String inputPath = parser.get("sourcePath");
		log.info("inputPath: {}", inputPath);

		final String outputPath = parser.get("outputPath");
		log.info("outputPath: {}", outputPath);

		SparkConf conf = new SparkConf();
		runWithSparkSession(
			conf,
			isSparkSessionManaged,
			spark -> {
				Utils.removeOutputDir(spark, outputPath);
				organizationMap(spark, inputPath, outputPath);

			});

	}

	private static void organizationMap(SparkSession spark, String inputPath, String outputPath) {
		Utils
			.readPath(spark, inputPath + "organization", eu.dnetlib.dhp.schema.oaf.Organization.class)
			.filter(
				(FilterFunction<eu.dnetlib.dhp.schema.oaf.Organization>) o -> !o.getDataInfo().getDeletedbyinference()
					&& o.getId().startsWith("20|openorgs"))
			.map(
				(MapFunction<eu.dnetlib.dhp.schema.oaf.Organization, Organization>) o -> mapOrganization(o),
				Encoders.bean(Organization.class))
			.filter((FilterFunction<Organization>) o -> o != null)
			.write()
			.mode(SaveMode.Overwrite)
			.option(COMPRESSION, GZIP)
			.json(outputPath + "/organization");
	}

	private static eu.dnetlib.dhp.oa.model.graph.Organization mapOrganization(
		eu.dnetlib.dhp.schema.oaf.Organization org) {

		Organization organization = new Organization();

		Optional
			.ofNullable(org.getLegalshortname())
			.ifPresent(value -> organization.setLegalShortName(value.getValue()));

		Optional
			.ofNullable(org.getLegalname())
			.ifPresent(value -> organization.setLegalName(value.getValue()));

		Optional
			.ofNullable(org.getWebsiteurl())
			.ifPresent(value -> organization.setWebsiteUrl(value.getValue()));

		Optional
			.ofNullable(org.getAlternativeNames())
			.ifPresent(
				value -> organization
					.setAlternativeNames(
						value
							.stream()
							.map(v -> v.getValue())
							.collect(Collectors.toList())));

		Optional
			.ofNullable(org.getCountry())
			.ifPresent(
				value -> {
					if (!value.getClassid().equals(eu.dnetlib.dhp.oa.graph.dump.complete.Constants.UNKNOWN)) {
						organization
							.setCountry(
								eu.dnetlib.dhp.oa.model.Country.newInstance(value.getClassid(), value.getClassname()));
					}

				});

		Optional
			.ofNullable(org.getId())
			.ifPresent(value -> organization.setId(getEntityId(value, ENTITY_ID_SEPARATOR)));

		Optional
			.ofNullable(org.getPid())
			.ifPresent(
				value -> organization
					.setPids(
						value
							.stream()
							.map(p -> OrganizationPid.newInstance(p.getQualifier().getClassid(), p.getValue()))
							.collect(Collectors.toList())));

		return organization;
	}

}
