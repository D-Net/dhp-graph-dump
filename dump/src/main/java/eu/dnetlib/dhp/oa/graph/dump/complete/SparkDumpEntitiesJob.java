
package eu.dnetlib.dhp.oa.graph.dump.complete;

import static eu.dnetlib.dhp.common.SparkSessionSupport.runWithSparkSession;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.*;
import static eu.dnetlib.dhp.oa.graph.dump.complete.DatasourceMapUtils.mapDatasource;
import static eu.dnetlib.dhp.oa.graph.dump.complete.OrganizationMapUtils.mapOrganization;
import static eu.dnetlib.dhp.oa.graph.dump.complete.ProjectMapUtils.mapProject;

import java.io.Serializable;
import java.io.StringReader;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.application.ArgumentApplicationParser;
import eu.dnetlib.dhp.oa.graph.dump.Constants;
import eu.dnetlib.dhp.oa.graph.dump.ResultMapper;
import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.graph.dump.community.CommunityMap;
import eu.dnetlib.dhp.oa.graph.dump.exceptions.CardinalityTooHighException;
import eu.dnetlib.dhp.oa.graph.dump.exceptions.NoAvailableEntityTypeException;
import eu.dnetlib.dhp.oa.model.Container;
import eu.dnetlib.dhp.oa.model.OrganizationPid;
import eu.dnetlib.dhp.oa.model.Result;
import eu.dnetlib.dhp.oa.model.graph.*;
import eu.dnetlib.dhp.oa.model.graph.Datasource;
import eu.dnetlib.dhp.oa.model.graph.Organization;
import eu.dnetlib.dhp.oa.model.graph.Project;
import eu.dnetlib.dhp.schema.common.ModelConstants;
import eu.dnetlib.dhp.schema.common.ModelSupport;
import eu.dnetlib.dhp.schema.oaf.*;
import eu.dnetlib.dhp.schema.oaf.H2020Classification;

/**
 * Spark Job that fires the dump for the entites
 */
public class SparkDumpEntitiesJob implements Serializable {
	private static final Logger log = LoggerFactory.getLogger(SparkDumpEntitiesJob.class);
	public static final String COMPRESSION = "compression";
	public static final String GZIP = "gzip";

	public static void main(String[] args) throws Exception {
		String jsonConfiguration = IOUtils
			.toString(
				SparkDumpEntitiesJob.class
					.getResourceAsStream(
						"/eu/dnetlib/dhp/oa/graph/dump/input_parameters.json"));

		final ArgumentApplicationParser parser = new ArgumentApplicationParser(jsonConfiguration);
		parser.parseArgument(args);

		Boolean isSparkSessionManaged = Optional
			.ofNullable(parser.get("isSparkSessionManaged"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);
		log.info("isSparkSessionManaged: {}", isSparkSessionManaged);

		final String inputPath = parser.get("sourcePath");
		log.info("inputPath: {}", inputPath);

		final String outputPath = parser.get("outputPath");
		log.info("outputPath: {}", outputPath);

		final String resultClassName = parser.get("resultTableName");
		log.info("resultTableName: {}", resultClassName);

		Optional<String> communityMap = Optional.ofNullable(parser.get("communityMapPath"));
		String communityMapPath = null;
		if (communityMap.isPresent())
			communityMapPath = communityMap.get();

		Class<? extends OafEntity> inputClazz = (Class<? extends OafEntity>) Class.forName(resultClassName);

		run(isSparkSessionManaged, inputPath, outputPath, communityMapPath, inputClazz);

	}

	private static void run(Boolean isSparkSessionManaged, String inputPath, String outputPath, String communityMapPath,
		Class<? extends OafEntity> inputClazz) {
		SparkConf conf = new SparkConf();
		conf.set("spark.sql.json.allowNull", "true");
		conf.set("spark.sql.jsonGenerator.ignoreNullFields", "false");
		switch (ModelSupport.idPrefixMap.get(inputClazz)) {
			case "50":
				String finalCommunityMapPath = communityMapPath;

				runWithSparkSession(
					conf,
					isSparkSessionManaged,
					spark -> {
						Utils.removeOutputDir(spark, outputPath);
						resultDump(
							spark, inputPath, outputPath, finalCommunityMapPath, inputClazz);
					});

				break;
			case "40":
				runWithSparkSession(
					conf,
					isSparkSessionManaged,
					spark -> {
						Utils.removeOutputDir(spark, outputPath);
						projectMap(spark, inputPath, outputPath, inputClazz);

					});
				break;
			case "20":
				runWithSparkSession(
					conf,
					isSparkSessionManaged,
					spark -> {
						Utils.removeOutputDir(spark, outputPath);
						organizationMap(spark, inputPath, outputPath, inputClazz);

					});
				break;
			case "10":
				runWithSparkSession(
					conf,
					isSparkSessionManaged,
					spark -> {
						Utils.removeOutputDir(spark, outputPath);
						datasourceMap(spark, inputPath, outputPath, inputClazz);

					});
				break;
		}
	}

	public static <I extends OafEntity> void resultDump(
		SparkSession spark,
		String inputPath,
		String outputPath,
		String communityMapPath,
		Class<I> inputClazz) {

		CommunityMap communityMap = null;
		if (!StringUtils.isEmpty(communityMapPath))
			communityMap = Utils.getCommunityMap(spark, communityMapPath);

		CommunityMap finalCommunityMap = communityMap;
		Utils
			.readPath(spark, inputPath, inputClazz)
			.map(
				(MapFunction<I, GraphResult>) value -> execMap(value, finalCommunityMap),
				Encoders.bean(GraphResult.class))
			.filter((FilterFunction<GraphResult>) value -> value != null)
			.write()
			.mode(SaveMode.Overwrite)
			.option(COMPRESSION, GZIP)
			.json(outputPath);

	}

	private static <I extends OafEntity, O extends Result> O execMap(I value,
		CommunityMap communityMap) throws NoAvailableEntityTypeException, CardinalityTooHighException {

		Optional<DataInfo> odInfo = Optional.ofNullable(value.getDataInfo());
		if (Boolean.FALSE.equals(odInfo.isPresent())) {
			return null;
		}
		if (Boolean.TRUE.equals(odInfo.get().getDeletedbyinference())
			|| Boolean.TRUE.equals(odInfo.get().getInvisible())) {
			return null;
		}

		return (O) ResultMapper.map(value, communityMap, Constants.DUMPTYPE.COMPLETE.getType());

	}

	private static <E extends OafEntity> void datasourceMap(SparkSession spark, String inputPath, String outputPath,
		Class<E> inputClazz) {
		Utils
			.readPath(spark, inputPath, inputClazz)
			.map(
				(MapFunction<E, Datasource>) d -> mapDatasource((eu.dnetlib.dhp.schema.oaf.Datasource) d),
				Encoders.bean(Datasource.class))
			.filter(Objects::nonNull)
			.map((MapFunction<Datasource, String>) r -> new ObjectMapper().writeValueAsString(r), Encoders.STRING())
			.write()
			.option(COMPRESSION, GZIP)
			.mode(SaveMode.Overwrite)
			.text(outputPath);
//			.write()
//			.mode(SaveMode.Overwrite)
//			.option(COMPRESSION, GZIP)
//			.json(outputPath);
	}

	private static <E extends OafEntity> void projectMap(SparkSession spark, String inputPath, String outputPath,
		Class<E> inputClazz) {
		Utils
			.readPath(spark, inputPath, inputClazz)
			.map(
				(MapFunction<E, Project>) p -> mapProject((eu.dnetlib.dhp.schema.oaf.Project) p),
				Encoders.bean(Project.class))
			.filter((FilterFunction<Project>) p -> p != null)
			.map((MapFunction<Project, String>) r -> new ObjectMapper().writeValueAsString(r), Encoders.STRING())
			.write()
			.option(COMPRESSION, GZIP)
			.mode(SaveMode.Overwrite)
			.text(outputPath);
//			.write()
//			.mode(SaveMode.Overwrite)
//			.option(COMPRESSION, GZIP)
//			.json(outputPath);
	}

	private static <E extends OafEntity> void organizationMap(SparkSession spark, String inputPath, String outputPath,
		Class<E> inputClazz) {
		Utils
			.readPath(spark, inputPath, inputClazz)
			.map(
				(MapFunction<E, Organization>) o -> mapOrganization((eu.dnetlib.dhp.schema.oaf.Organization) o),
				Encoders.bean(Organization.class))
			.filter((FilterFunction<Organization>) o -> o != null)
			.map((MapFunction<Organization, String>) r -> new ObjectMapper().writeValueAsString(r), Encoders.STRING())
			.write()
			.option(COMPRESSION, GZIP)
			.mode(SaveMode.Overwrite)
			.text(outputPath);
//			.write()
//			.mode(SaveMode.Overwrite)
//			.option(COMPRESSION, GZIP)
//			.json(outputPath);
	}

}
