
package eu.dnetlib.dhp.oa.graph.dump.csv.model;

import java.io.Serializable;

import eu.dnetlib.dhp.oa.graph.dump.csv.Constants;

/**
 * @author miriam.baglioni
 * @Date 11/05/23
 */
public class CSVRELCommunityResult implements Serializable {
	private String result_id;
	private String community_id;

	public String getResult_id() {
		return result_id;
	}

	public void setResult_id(String result_id) {
		this.result_id = Constants.addQuotes(result_id);
	}

	public String getCommunity_id() {
		return community_id;
	}

	public void setCommunity_id(String community_id) {
		this.community_id = Constants.addQuotes(community_id);
	}
}
