
package eu.dnetlib.dhp.oa.graph.dump.complete;

import static eu.dnetlib.dhp.common.SparkSessionSupport.runWithSparkSession;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.ENTITY_ID_SEPARATOR;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.getEntityId;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.dnetlib.dhp.application.ArgumentApplicationParser;
import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.Provenance;
import eu.dnetlib.dhp.oa.model.graph.RelType;
import eu.dnetlib.dhp.schema.common.ModelSupport;
import eu.dnetlib.dhp.schema.oaf.DataInfo;
import eu.dnetlib.dhp.schema.oaf.Relation;

/**
 * Dumps eu.dnetlib.dhp.schema.oaf.Relation in eu.dnetlib.dhp.schema.dump.oaf.graph.Relation
 */
public class SparkDumpRelationJob implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(SparkDumpRelationJob.class);

	public static void main(String[] args) throws Exception {
		String jsonConfiguration = IOUtils
			.toString(
				SparkDumpRelationJob.class
					.getResourceAsStream(
						"/eu/dnetlib/dhp/oa/graph/dump/input_relationdump_parameters.json"));

		final ArgumentApplicationParser parser = new ArgumentApplicationParser(jsonConfiguration);
		parser.parseArgument(args);

		Boolean isSparkSessionManaged = Optional
			.ofNullable(parser.get("isSparkSessionManaged"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);
		log.info("isSparkSessionManaged: {}", isSparkSessionManaged);

		final String inputPath = parser.get("sourcePath");
		log.info("inputPath: {}", inputPath);

		final String outputPath = parser.get("outputPath");
		log.info("outputPath: {}", outputPath);

		Optional<String> rs = Optional.ofNullable(parser.get("removeSet"));
		final Set<String> removeSet = new HashSet<>();
		if (rs.isPresent()) {
			Collections.addAll(removeSet, rs.get().split(";"));
		}

		SparkConf conf = new SparkConf();

		runWithSparkSession(
			conf,
			isSparkSessionManaged,
			spark -> {
				Utils.removeOutputDir(spark, outputPath);
				dumpRelation(spark, inputPath, outputPath, removeSet);

			});

	}

	private static void dumpRelation(SparkSession spark, String inputPath, String outputPath, Set<String> removeSet) {
		Dataset<Relation> relations = Utils.readPath(spark, inputPath, Relation.class);
		relations
			.filter(
				(FilterFunction<Relation>) r -> !removeSet.contains(r.getRelClass())
					&& !r.getSubRelType().equalsIgnoreCase("resultService"))
			.map((MapFunction<Relation, eu.dnetlib.dhp.oa.model.graph.Relation>) relation -> {
				eu.dnetlib.dhp.oa.model.graph.Relation relNew = new eu.dnetlib.dhp.oa.model.graph.Relation();
				relNew
					.setSource(getEntityId(relation.getSource(), ENTITY_ID_SEPARATOR));
				relNew.setSourceType(getEntityType(relation.getSource().substring(0, 2)));

				relNew
					.setTarget(getEntityId(relation.getTarget(), ENTITY_ID_SEPARATOR));
				relNew.setTargetType(getEntityType(relation.getTarget().substring(0, 2)));

				relNew
					.setRelType(
						RelType
							.newInstance(
								relation.getRelClass(),
								relation.getSubRelType()));

				Optional<DataInfo> odInfo = Optional.ofNullable(relation.getDataInfo());
				if (odInfo.isPresent()) {
					DataInfo dInfo = odInfo.get();
					if (Optional.ofNullable(dInfo.getProvenanceaction()).isPresent() &&
						Optional.ofNullable(dInfo.getProvenanceaction().getClassname()).isPresent()) {
						relNew
							.setProvenance(
								Provenance
									.newInstance(
										dInfo.getProvenanceaction().getClassname(),
										dInfo.getTrust()));
					}
				}
				if (Boolean.TRUE.equals(relation.getValidated())) {
					relNew.setValidated(relation.getValidated());
					relNew.setValidationDate(relation.getValidationDate());
				}

				return relNew;

			}, Encoders.bean(eu.dnetlib.dhp.oa.model.graph.Relation.class))
			.write()
			.option("compression", "gzip")
			.mode(SaveMode.Append)
			.json(outputPath);

	}

	private static String getEntityType(final String entityPrefix) {
		String type = ModelSupport.idPrefixEntity.get(entityPrefix);
		if ("result".equals(type))
			return "product";
		return type;
	}

}
