
package eu.dnetlib.dhp.oa.graph.dump.complete;

import static eu.dnetlib.dhp.oa.graph.dump.Utils.*;

import java.util.Optional;

import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.OrganizationPid;
import eu.dnetlib.dhp.oa.model.graph.Organization;
import eu.dnetlib.dhp.schema.common.ModelConstants;
import eu.dnetlib.dhp.schema.oaf.Field;
import eu.dnetlib.dhp.schema.oaf.Qualifier;

public class OrganizationMapUtils {
	public static eu.dnetlib.dhp.oa.model.graph.Organization mapOrganization(
		eu.dnetlib.dhp.schema.oaf.Organization org) {
		if (Boolean.TRUE.equals(org.getDataInfo().getDeletedbyinference()))
			return null;
		if (!Optional.ofNullable(org.getLegalname()).isPresent()
			&& !Optional.ofNullable(org.getLegalshortname()).isPresent())
			return null;

		Organization organization = new Organization();

		organization.setLegalShortName(mapFieldString(org.getLegalshortname()));
		organization.setLegalName(mapFieldString(org.getLegalname()));
		organization.setWebsiteUrl(mapFieldString(org.getWebsiteurl()));
		organization.setAlternativeNames(Utils.mapListString(org.getAlternativeNames(), Field::getValue));
		organization.setCountry(mapCountry(org.getCountry()));
		organization.setId(getEntityId(org.getId(), ENTITY_ID_SEPARATOR));
		organization
			.setPids(
				mapPid(org.getPid(), p -> OrganizationPid.newInstance(p.getQualifier().getClassid(), p.getValue())));
		return organization;
	}

	private static eu.dnetlib.dhp.oa.model.Country mapCountry(Qualifier country) {
		if (country == null)
			return null;
		if (country.getClassid() == null || country.getClassid().trim().isEmpty()
			|| country.getClassid().equalsIgnoreCase(ModelConstants.UNKNOWN))
			return null;
		return eu.dnetlib.dhp.oa.model.Country.newInstance(country.getClassid(), country.getClassname());

	}
}
