
package eu.dnetlib.dhp.oa.zenodoapi.model;

/**
 * @author miriam.baglioni
 * @Date 01/07/23
 */
public class Community {
	private String identifier;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
}
