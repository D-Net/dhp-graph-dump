
package eu.dnetlib.dhp.oa.graph.dump.subset.selectionconstraints;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import eu.dnetlib.dhp.oa.graph.dump.subset.criteria.VerbResolver;

public class SelectionConstraints implements Serializable {
	private List<Constraints> criteria;

	public List<Constraints> getCriteria() {
		return criteria;
	}

	public void setCriteria(List<Constraints> criteria) {
		this.criteria = criteria;
	}

	public void setSc(String json) {
		Type collectionType = new TypeToken<Collection<Constraints>>() {
		}.getType();
		criteria = new Gson().fromJson(json, collectionType);
	}

	// Constraints in or
	public boolean verifyCriteria(final Param param) {
		for (Constraints selc : criteria) {
			if (selc.verifyCriteria(param)) {
				return true;
			}
		}
		return false;
	}

	public void addResolver(VerbResolver resolver) {

		for (Constraints cs : criteria) {
			cs.setSelection(resolver);
		}
	}
}
