
package eu.dnetlib.dhp.oa.graph.dump;

import static eu.dnetlib.dhp.oa.graph.dump.Constants.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.cxf.common.util.CollectionUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.jetbrains.annotations.NotNull;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.dnetlib.dhp.common.HdfsSupport;
import eu.dnetlib.dhp.oa.graph.dump.community.CommunityMap;
import eu.dnetlib.dhp.oa.graph.dump.complete.Constants;
import eu.dnetlib.dhp.oa.model.BipIndicators;
import eu.dnetlib.dhp.oa.model.Container;
import eu.dnetlib.dhp.oa.model.Indicator;
import eu.dnetlib.dhp.oa.model.UsageCounts;
import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.dnetlib.dhp.oa.model.graph.Relation;
import eu.dnetlib.dhp.oa.model.graph.ResearchCommunity;
import eu.dnetlib.dhp.schema.oaf.*;
import eu.dnetlib.dhp.utils.DHPUtils;
import scala.Tuple2;

public class Utils {
	public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
		.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	public static final String ENTITY_ID_SEPARATOR = "|";

	private Utils() {
	}

	public static void removeOutputDir(SparkSession spark, String path) {
		HdfsSupport.remove(path, spark.sparkContext().hadoopConfiguration());
	}

	public static <R> Dataset<R> readPath(
		SparkSession spark, String inputPath, Class<R> clazz) {
		return spark
			.read()
			.textFile(inputPath)
			.map((MapFunction<String, R>) value -> OBJECT_MAPPER.readValue(value, clazz), Encoders.bean(clazz));
	}

	public static String getContextId(String id) {

		return String
			.format(
				"%s::%s", Constants.CONTEXT_NS_PREFIX,
				DHPUtils.md5(id));
	}

	public static CommunityMap getCommunityMap(SparkSession spark, String communityMapPath) {

		return new Gson().fromJson(spark.read().textFile(communityMapPath).collectAsList().get(0), CommunityMap.class);

	}

	public static CommunityMap readCommunityMap(FileSystem fileSystem, String communityMapPath) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(fileSystem.open(new Path(communityMapPath))));
		StringBuilder sb = new StringBuilder();
		try {
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} finally {
			br.close();

		}

		return new Gson().fromJson(sb.toString(), CommunityMap.class);
	}

	public static String getEntityId(String id, String separator) {
		return id.substring(id.indexOf(separator) + 1);
	}

	public static Dataset<String> getEntitiesId(SparkSession spark, String inputPath) {
		Dataset<String> dumpedIds;
		dumpedIds = Utils
			.readPath(spark, inputPath + "/publication", GraphResult.class)
			.map((MapFunction<GraphResult, String>) r -> r.getId(), Encoders.STRING())
			.union(
				Utils
					.readPath(spark, inputPath + "/dataset", GraphResult.class)
					.map((MapFunction<GraphResult, String>) r -> r.getId(), Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/software", GraphResult.class)
					.map((MapFunction<GraphResult, String>) r -> r.getId(), Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/otherresearchproduct", GraphResult.class)
					.map((MapFunction<GraphResult, String>) r -> r.getId(), Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/organization", eu.dnetlib.dhp.oa.model.graph.Organization.class)
					.map(
						(MapFunction<eu.dnetlib.dhp.oa.model.graph.Organization, String>) o -> o.getId(),
						Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/project", eu.dnetlib.dhp.oa.model.graph.Project.class)
					.map(
						(MapFunction<eu.dnetlib.dhp.oa.model.graph.Project, String>) o -> o.getId(), Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/datasource", eu.dnetlib.dhp.oa.model.graph.Datasource.class)
					.map(
						(MapFunction<eu.dnetlib.dhp.oa.model.graph.Datasource, String>) o -> o.getId(),
						Encoders.STRING()))
			.union(
				Utils
					.readPath(spark, inputPath + "/communities_infrastructures", ResearchCommunity.class)
					.map((MapFunction<ResearchCommunity, String>) c -> c.getId(), Encoders.STRING()));
		return dumpedIds;
	}

	public static Dataset<Relation> getValidRelations(Dataset<Relation> relations,
		Dataset<String> entitiesIds) {
		Dataset<Tuple2<String, Relation>> relationSource = relations
			.map(
				(MapFunction<Relation, Tuple2<String, Relation>>) r -> new Tuple2<>(r.getSource(), r),
				Encoders.tuple(Encoders.STRING(), Encoders.bean(Relation.class)));

		Dataset<Tuple2<String, Relation>> relJoinSource = relationSource
			.joinWith(entitiesIds, relationSource.col("_1").equalTo(entitiesIds.col("value")))
			.map(
				(MapFunction<Tuple2<Tuple2<String, Relation>, String>, Tuple2<String, Relation>>) t2 -> new Tuple2<>(
					t2._1()._2().getTarget(), t2._1()._2()),
				Encoders.tuple(Encoders.STRING(), Encoders.bean(Relation.class)));

		return relJoinSource
			.joinWith(entitiesIds, relJoinSource.col("_1").equalTo(entitiesIds.col("value")))
			.map(
				(MapFunction<Tuple2<Tuple2<String, Relation>, String>, Relation>) t2 -> t2._1()._2(),
				Encoders.bean(Relation.class));
	}

	/**
	 * @param measures
	 * @return
	 */
	public static Indicator getIndicator(List<Measure> measures) {
		Indicator i = new Indicator();
		for (eu.dnetlib.dhp.schema.oaf.Measure m : measures) {
			switch (m.getId()) {
				case USAGE_COUNT_DOWNLOADS:
					getUsageCounts(i).setDownloads(Integer.parseInt(m.getUnit().get(0).getValue()));
					break;
				case USAGE_COUNT_VIEWS:
					getUsageCounts(i).setViews(Integer.parseInt(m.getUnit().get(0).getValue()));
					break;
				case BIP_INFLUENCE:
					m.getUnit().forEach(u -> {
						if (u.getKey().equals("class"))
							getImpactMeasure(i).setInfluenceClass(u.getValue());
						if (u.getKey().equals("score"))
							getImpactMeasure(i).setInfluence(Double.parseDouble(u.getValue()));
					});
					break;
				case BIP_POPULARITY:
					m.getUnit().forEach(u -> {
						if (u.getKey().equals("class"))
							getImpactMeasure(i).setPopularityClass(u.getValue());
						if (u.getKey().equals("score"))
							getImpactMeasure(i).setPopularity(Double.parseDouble(u.getValue()));
					});
					break;
				case BIP_INFLUENCE_ALT:
					m.getUnit().forEach(u -> {
						if (u.getKey().equals("class"))
							getImpactMeasure(i).setCitationClass(u.getValue());
						if (u.getKey().equals("score"))
							getImpactMeasure(i).setCitationCount(Double.parseDouble(u.getValue()));
					});
					break;
				case BIP_POPULARITY_ALT:
					break;
				case BIP_IMPULSE:
					m.getUnit().forEach(u -> {
						if (u.getKey().equals("class"))
							getImpactMeasure(i).setImpulseClass(u.getValue());
						if (u.getKey().equals("score"))
							getImpactMeasure(i).setImpulse(Double.parseDouble(u.getValue()));
					});
					break;
				default:
					throw new RuntimeException("No mapping found for indicator " + m.getId());

			}
		}

		return i;
	}

	@NotNull
	private static UsageCounts getUsageCounts(Indicator i) {
		if (i.getUsageCounts() == null) {
			i.setUsageCounts(new UsageCounts());
		}
		return i.getUsageCounts();
	}

	@NotNull
	private static BipIndicators getImpactMeasure(Indicator i) {
		if (i.getCitationImpact() == null) {
			i.setCitationImpact(new BipIndicators());
		}
		return i.getCitationImpact();
	}

	private static <T> T mapField(Field<T> in, Function<T, Boolean> validator) {
		if (in == null || !validator.apply(in.getValue())) {
			return null; // Return null if the field or its value is null or invalid
		}
		return in.getValue(); // Return the value if it's valid
	}

	public static String mapFieldString(Field<String> in) {
		if (in == null || in.getValue() == null || in.getValue().trim().isEmpty()) {
			return null; // Return null if the field or its value is null
		}

		return in.getValue();

	}

	public static Boolean mapFieldBool(Field<Boolean> in) {
		if (in == null || in.getValue() == null) {
			return null; // Return null if the field or its value is null
		}

		return in.getValue();

	}

	public static <T> List<String> mapListString(List<T> in, Function<T, String> mapper) {
		if (CollectionUtils.isEmpty(in)) {
			return null; // Restituisce null se la lista di input è vuota o nulla
		}
		List<String> result = in
			.stream()
			.map(mapper)
			.filter(Objects::nonNull)
			.filter(s -> !s.trim().isEmpty())// Filtro per null se necessario
			.collect(Collectors.toList());
		return CollectionUtils.isEmpty(result) ? null : result; // Restituisce null se la lista risultante è vuota
	}

	public static String mapQualifierName(Qualifier in) {

		return in == null || in.getClassname() == null || in.getClassname().trim().isEmpty() ? null : in.getClassname();

	}

	public static <T> List<T> mapPid(List<StructuredProperty> pid, Function<StructuredProperty, T> mapper) {
		if (pid == null || pid.isEmpty())
			return null;
		List<T> lista = pid
			.stream()
			.map(mapper)
			.collect(Collectors.toList());
		return CollectionUtils.isEmpty(lista) ? null : lista;
	}

	private static String mapString(String in) {
		return (in == null || in.trim().isEmpty()) ? null : in;
	}

	public static @NotNull Container mapContainer(Journal j) {

		Container c = null;
		if (j != null) {
			c = new Container();
			// c.setConferenceDate(j.getConferencedate());

			c.setConferencePlace(mapString(j.getConferenceplace()));
			c.setEdition(mapString(j.getEdition()));
			c.setEp(mapString(j.getEp()));
			c.setIss(mapString(j.getIss()));
			c.setIssnLinking(mapString(j.getIssnLinking()));
			c.setIssnOnline(mapString(j.getIssnOnline()));
			c.setIssnPrinted(mapString(j.getIssnPrinted()));
			c.setName(mapString(j.getName()));
			c.setSp(mapString(j.getSp()));
			c.setVol(mapString(j.getVol()));
		}

		return c;
	}
}
