
package eu.dnetlib.dhp.oa.zenodoapi.model;

/**
 * @author miriam.baglioni
 * @Date 01/07/23
 */
/**
 * @author miriam.baglioni
 * @Date 01/07/23
 */
import java.io.Serializable;

public class File implements Serializable {
	private String checksum;
	private String filename;
	private long filesize;
	private String id;

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public long getFilesize() {
		return filesize;
	}

	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
