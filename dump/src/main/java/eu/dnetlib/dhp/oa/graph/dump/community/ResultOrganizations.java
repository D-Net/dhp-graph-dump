
package eu.dnetlib.dhp.oa.graph.dump.community;

import java.io.Serializable;

import eu.dnetlib.dhp.oa.model.community.Affiliation;

/**
 * @author miriam.baglioni
 * @Date 20/09/22
 */
public class ResultOrganizations implements Serializable {
	private String resultId;
	private Affiliation affiliation;

	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

	public Affiliation getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(Affiliation affiliation) {
		this.affiliation = affiliation;
	}
}
