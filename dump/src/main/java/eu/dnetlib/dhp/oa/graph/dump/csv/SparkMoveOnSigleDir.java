
package eu.dnetlib.dhp.oa.graph.dump.csv;

import static eu.dnetlib.dhp.common.SparkSessionSupport.runWithSparkSession;

import java.io.Serializable;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.MapGroupsFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.dnetlib.dhp.application.ArgumentApplicationParser;
import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.graph.dump.csv.model.CSVAuthor;
import eu.dnetlib.dhp.oa.graph.dump.csv.model.CSVPid;
import eu.dnetlib.dhp.oa.graph.dump.csv.model.CSVRelResAut;
import eu.dnetlib.dhp.oa.graph.dump.csv.model.CSVResult;
import eu.dnetlib.dhp.schema.oaf.*;

/**
 * @author miriam.baglioni
 * @Date 10/05/23
 */
//STEP 4
public class SparkMoveOnSigleDir implements Serializable {

	// All the products saved in different directories are put under the same one.
	// For the authors also a step of reconciliation mast be done, since the same author id can be saved in more than
	// one directory

	private static final Logger log = LoggerFactory.getLogger(SparkMoveOnSigleDir.class);

	public static void main(String[] args) throws Exception {
		String jsonConfiguration = IOUtils
			.toString(
				SparkMoveOnSigleDir.class
					.getResourceAsStream(
						"/eu/dnetlib/dhp/oa/graph/dump/input_dump_csv_ste4.json"));

		final ArgumentApplicationParser parser = new ArgumentApplicationParser(jsonConfiguration);
		parser.parseArgument(args);

		Boolean isSparkSessionManaged = Optional
			.ofNullable(parser.get("isSparkSessionManaged"))
			.map(Boolean::valueOf)
			.orElse(Boolean.TRUE);
		log.info("isSparkSessionManaged: {}", isSparkSessionManaged);

		final String workingPath = parser.get("workingPath");
		log.info("workingPath: {}", workingPath);

		final String outputPath = parser.get("outputPath");
		log.info("outputPath: {}", outputPath);

		SparkConf conf = new SparkConf();

		runWithSparkSession(
			conf,
			isSparkSessionManaged,
			spark -> {
				// Utils.removeOutputDir(spark, outputPath);
				run(spark, outputPath, workingPath);

			});

	}

	private static <R extends Result> void run(SparkSession spark, String outputPath,
		String workingPath) {

		Utils
			.readPath(spark, workingPath + "/publication/result", CSVResult.class)
			.union(Utils.readPath(spark, workingPath + "/dataset/result", CSVResult.class))
			.union(Utils.readPath(spark, workingPath + "/software/result", CSVResult.class))
			.union(Utils.readPath(spark, workingPath + "/otherresearchproduct/result", CSVResult.class))
			.write()
			.mode(SaveMode.Overwrite)
			.option("header", "true")
			.option("delimiter", Constants.SEP)
			.option("compression", "gzip")
			.csv(outputPath + "/result");

		Utils
			.readPath(spark, workingPath + "/publication/result_pid", CSVPid.class)
			.union(Utils.readPath(spark, workingPath + "/dataset/result_pid", CSVPid.class))
			.union(Utils.readPath(spark, workingPath + "/software/result_pid", CSVPid.class))
			.union(Utils.readPath(spark, workingPath + "/otherresearchproduct/result_pid", CSVPid.class))
			.write()
			.mode(SaveMode.Overwrite)
			.option("header", "true")
			.option("delimiter", Constants.SEP)
			.option("compression", "gzip")
			.csv(outputPath + "/result_pid");

		Utils
			.readPath(spark, workingPath + "/publication/result_author", CSVRelResAut.class)
			.union(Utils.readPath(spark, workingPath + "/dataset/result_author", CSVRelResAut.class))
			.union(Utils.readPath(spark, workingPath + "/software/result_author", CSVRelResAut.class))
			.union(Utils.readPath(spark, workingPath + "/otherresearchproduct/result_author", CSVRelResAut.class))
			.write()
			.mode(SaveMode.Overwrite)
			.option("header", "true")
			.option("delimiter", Constants.SEP)
			.option("compression", "gzip")
			.csv(outputPath + "/result_author");

		Utils
			.readPath(spark, workingPath + "/publication/author", CSVAuthor.class)
			.union(Utils.readPath(spark, workingPath + "/dataset/author", CSVAuthor.class))
			.union(Utils.readPath(spark, workingPath + "/software/author", CSVAuthor.class))
			.union(Utils.readPath(spark, workingPath + "/otherresearchproduct/author", CSVAuthor.class))
			.groupByKey((MapFunction<CSVAuthor, String>) r -> r.getId(), Encoders.STRING())
			.mapGroups(
				(MapGroupsFunction<String, CSVAuthor, CSVAuthor>) (k, it) -> it.next(), Encoders.bean(CSVAuthor.class))
			.write()
			.mode(SaveMode.Overwrite)
			.option("header", "true")
			.option("delimiter", Constants.SEP)
			.option("compression", "gzip")
			.csv(outputPath + "/author");

	}

}
