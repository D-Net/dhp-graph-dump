
package eu.dnetlib.dhp.oa.graph.dump;

import static eu.dnetlib.dhp.oa.graph.dump.Constants.*;
import static eu.dnetlib.dhp.oa.graph.dump.Utils.*;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.common.util.CollectionUtils;
import org.jetbrains.annotations.NotNull;

import eu.dnetlib.dhp.oa.graph.dump.exceptions.CardinalityTooHighException;
import eu.dnetlib.dhp.oa.graph.dump.exceptions.NoAvailableEntityTypeException;
import eu.dnetlib.dhp.oa.model.*;
import eu.dnetlib.dhp.oa.model.AccessRight;
import eu.dnetlib.dhp.oa.model.Author;
import eu.dnetlib.dhp.oa.model.GeoLocation;
import eu.dnetlib.dhp.oa.model.Instance;
import eu.dnetlib.dhp.oa.model.OpenAccessColor;
import eu.dnetlib.dhp.oa.model.OpenAccessRoute;
import eu.dnetlib.dhp.oa.model.Result;
import eu.dnetlib.dhp.oa.model.Subject;
import eu.dnetlib.dhp.oa.model.community.CfHbKeyValue;
import eu.dnetlib.dhp.oa.model.community.CommunityInstance;
import eu.dnetlib.dhp.oa.model.community.CommunityResult;
import eu.dnetlib.dhp.oa.model.community.Context;
import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.dnetlib.dhp.schema.common.ModelConstants;
import eu.dnetlib.dhp.schema.oaf.*;

public class ResultMapper implements Serializable {
	private static final String NULL = "null";

	public static <E extends eu.dnetlib.dhp.schema.oaf.OafEntity> Result map(
		E in, Map<String, String> communityMap, String dumpType)
		throws NoAvailableEntityTypeException, CardinalityTooHighException {

		Result out;
		if (Constants.DUMPTYPE.COMPLETE.getType().equals(dumpType)) {
			out = new GraphResult();
		} else {
			out = new CommunityResult();
		}

		eu.dnetlib.dhp.schema.oaf.Result input = (eu.dnetlib.dhp.schema.oaf.Result) in;
		Optional<eu.dnetlib.dhp.schema.oaf.Qualifier> ort = Optional.ofNullable(input.getResulttype());
		if (ort.isPresent()) {
			try {

				addTypeSpecificInformation(out, input, ort.get());
				out.setAuthors(mapAuthor(input.getAuthor()));
				mapAccessRight(out, input);
				out.setContributors(mapListString(input.getContributor(), Field::getValue));
				out.setPublicationDate(mapFieldString(input.getDateofacceptance()));
				mapCountry(out, input);
				out.setCoverages(mapListString(input.getCoverage(), Field::getValue));
				out.setDateOfCollection(input.getDateofcollection());
				out.setIsGreen(input.getIsGreen());
				out.setIsInDiamondJournal(input.getIsInDiamondJournal());
				out.setPubliclyFunded(input.getPubliclyFunded());
				mapOpenAccessColor(out, input);
				out.setDescriptions(mapListString(input.getDescription(), Field::getValue));
				out.setEmbargoEndDate(mapFieldString(input.getEmbargoenddate()));
				out.setFormats(mapListString(input.getFormat(), Field::getValue));
				out.setId(getEntityId(input.getId(), ENTITY_ID_SEPARATOR));
				out.setOriginalIds(mapListString(input.getOriginalId(), v -> v));
				mapInstance(dumpType, out, input);
				mapLanguage(out, input);
				out.setLastUpdateTimeStamp(input.getLastupdatetimestamp());
				mapTitle(out, input);
				out
					.setPids(
						Utils
							.mapPid(
								input.getPid(),
								p -> ResultPid.newInstance(p.getQualifier().getClassid(), p.getValue())));
				out.setPublisher(mapFieldString(input.getPublisher()));
				out.setSources(mapListString(input.getSource(), Field::getValue));
				mapSubject(out, input);
				out.setType(input.getResulttype().getClassid());
				mapMeasure(out, input);
				if (!Constants.DUMPTYPE.COMPLETE.getType().equals(dumpType)) {
					mapCollectedfrom((CommunityResult) out, input);
					mapContext(communityMap, (CommunityResult) out, input);
				}
			} catch (ClassCastException cce) {
				return null;
			}
		}

		return out;

	}

	private static void mapOpenAccessColor(Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		out.setOpenAccessColor(null);
		if (Optional.ofNullable(input.getOpenAccessColor()).isPresent())
			switch (input.getOpenAccessColor()) {
				case bronze:
					out.setOpenAccessColor(OpenAccessColor.bronze);
					break;
				case gold:
					out.setOpenAccessColor(OpenAccessColor.gold);
					break;
				case hybrid:
					out.setOpenAccessColor(OpenAccessColor.hybrid);
					break;

			}

	}

	private static void mapContext(Map<String, String> communityMap, CommunityResult out,
		eu.dnetlib.dhp.schema.oaf.Result input) {
		Set<String> communities = communityMap.keySet();
		List<Context> contextList = Optional
			.ofNullable(
				input
					.getContext())
			.map(
				value -> value
					.stream()
					.map(c -> {
						String communityId = c.getId();
						if (communityId.contains("::")) {
							communityId = communityId.substring(0, communityId.indexOf("::"));
						}
						if (communities.contains(communityId)) {
							Context context = new Context();
							context.setCode(communityId);
							context.setLabel(communityMap.get(communityId));
							Optional<List<DataInfo>> dataInfo = Optional.ofNullable(c.getDataInfo());
							if (dataInfo.isPresent()) {
								List<Provenance> provenance = new ArrayList<>(dataInfo
									.get()
									.stream()
									.map(
										di -> Optional
											.ofNullable(di.getProvenanceaction())
											.map(
												provenanceaction -> Provenance
													.newInstance(
														provenanceaction.getClassname(),
														di.getTrust()))
											.orElse(null))
									.filter(Objects::nonNull)
									.collect(Collectors.toSet()));

								try {
									context.setProvenance(getUniqueProvenance(provenance));
									if (context.getProvenance().isEmpty())
										context.setProvenance(null);
								} catch (NoAvailableEntityTypeException e) {
									e.printStackTrace();
								}
							}
							return context;
						}
						return null;
					})
					.filter(Objects::nonNull)
					.collect(Collectors.toList()))
			.orElse(Collections.emptyList());

		if (!contextList.isEmpty()) {
			Set<Integer> hashValue = new HashSet<>();
			List<Context> remainigContext = new ArrayList<>();
			contextList.forEach(c -> {
				if (!hashValue.contains(c.hashCode())) {
					remainigContext.add(c);
					hashValue.add(c.hashCode());
				}
			});
			if (!remainigContext.isEmpty())
				out.setCommunities(remainigContext);
			else
				out.setCommunities(null);
		}
	}

	private static void mapCollectedfrom(CommunityResult out, eu.dnetlib.dhp.schema.oaf.Result input) {
		out
			.setCollectedFrom(
				input
					.getCollectedfrom()
					.stream()
					.map(cf -> CfHbKeyValue.newInstance(getEntityId(cf.getKey(), ENTITY_ID_SEPARATOR), cf.getValue()))
					.collect(Collectors.toList()));
		if (out.getCollectedFrom().isEmpty())
			out.setCollectedFrom(null);
	}

	private static void mapMeasure(Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		if (Optional.ofNullable(input.getMeasures()).isPresent() && input.getMeasures().size() > 0) {

			out.setIndicators(Utils.getIndicator(input.getMeasures()));
		} else
			out.setIndicators(null);
	}

	private static void mapSubject(Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		List<Subject> subjectList = new ArrayList<>();
		Optional
			.ofNullable(input.getSubject())
			.ifPresent(
				value -> value
					.stream()
					.filter(
						s -> !(s.getQualifier().getClassid().equalsIgnoreCase("fos")) ||
							(s.getQualifier().getClassid().equalsIgnoreCase("fos") &&
								s.getValue().split(" ")[0].trim().length() < 5))
//					.filter(
//						s -> !((s.getQualifier().getClassid().equalsIgnoreCase("fos") &&
//							Optional.ofNullable(s.getDataInfo()).isPresent()
//							&& Optional.ofNullable(s.getDataInfo().getProvenanceaction()).isPresent() &&
//							s.getDataInfo().getProvenanceaction().getClassid().equalsIgnoreCase("subject:fos"))
//							||
//							(s.getQualifier().getClassid().equalsIgnoreCase("sdg") &&
//								Optional.ofNullable(s.getDataInfo()).isPresent()
//								&& Optional.ofNullable(s.getDataInfo().getProvenanceaction()).isPresent() &&
//								s
//									.getDataInfo()
//									.getProvenanceaction()
//									.getClassid()
//									.equalsIgnoreCase("subject:sdg"))))
					.filter(s -> !s.getValue().equalsIgnoreCase(NULL))
					.forEach(s -> subjectList.add(getSubject(s))));
		if (subjectList.isEmpty())
			out.setSubjects(null);
		else
			out.setSubjects(subjectList);
	}

	private static void mapTitle(Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		Optional<List<StructuredProperty>> otitle = Optional.ofNullable(input.getTitle());
		if (otitle.isPresent()) {
			List<StructuredProperty> iTitle = otitle
				.get()
				.stream()
				.filter(t -> t.getQualifier().getClassid().equalsIgnoreCase("main title"))
				.collect(Collectors.toList());
			if (!iTitle.isEmpty()) {
				out.setMainTitle(iTitle.get(0).getValue());
			} else
				out.setMainTitle(null);

			iTitle = otitle
				.get()
				.stream()
				.filter(t -> t.getQualifier().getClassid().equalsIgnoreCase("subtitle"))
				.collect(Collectors.toList());
			if (!iTitle.isEmpty()) {
				out.setSubTitle(iTitle.get(0).getValue());
			} else
				out.setSubTitle(null);

		}
	}

	private static void mapLanguage(Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		Optional<Qualifier> oL = Optional.ofNullable(input.getLanguage());
		if (oL.isPresent()) {
			Qualifier language = oL.get();
			out.setLanguage(Language.newInstance(language.getClassid(), language.getClassname()));
		} else
			out.setLanguage(null);
	}

	private static void mapInstance(String dumpType, Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		Optional<List<eu.dnetlib.dhp.schema.oaf.Instance>> oInst = Optional
			.ofNullable(input.getInstance());

		if (oInst.isPresent()) {
			if (DUMPTYPE.COMPLETE.getType().equals(dumpType)) {
				((GraphResult) out)
					.setInstances(
						oInst.get().stream().map(ResultMapper::getGraphInstance).collect(Collectors.toList()));
				if (((GraphResult) out).getInstances().isEmpty())
					((GraphResult) out).setInstances(null);
			} else {
				((CommunityResult) out)
					.setInstances(
						oInst
							.get()
							.stream()
							.map(ResultMapper::getCommunityInstance)
							.collect(Collectors.toList()));
				if (((CommunityResult) out).getInstances().isEmpty())
					((CommunityResult) out).setInstances(null);
			}
		}

	}

	private static void mapOriginalId(Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		out.setOriginalIds(null);
		Optional
			.ofNullable(input.getOriginalId())
			.ifPresent(
				v -> out
					.setOriginalIds(
						input
							.getOriginalId()
							.stream()
							.filter(s -> !s.startsWith("50|"))
							.collect(Collectors.toList())));
	}

	private static void mapCountry(Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		Optional
			.ofNullable(input.getCountry())
			.ifPresent(
				value -> out
					.setCountries(
						value
							.stream()
							.map(
								c -> {
									if (c.getClassid().equals((ModelConstants.UNKNOWN))) {
										return null;
									}
									ResultCountry country = new ResultCountry();
									country.setCode(c.getClassid());
									country.setLabel(c.getClassname());
									Optional
										.ofNullable(c.getDataInfo())
										.ifPresent(
											provenance -> country
												.setProvenance(
													Provenance
														.newInstance(
															provenance
																.getProvenanceaction()
																.getClassname(),
															c.getDataInfo().getTrust())));
									return country;
								})
							.filter(Objects::nonNull)
							.collect(Collectors.toList())));
		if (out.getCountries() == null || out.getCountries().isEmpty())
			out.setCountries(null);
	}

	private static void mapAccessRight(Result out, eu.dnetlib.dhp.schema.oaf.Result input) {
		// I do not map Access Right UNKNOWN or OTHER

		Optional<Qualifier> oar = Optional.ofNullable(input.getBestaccessright());
		if (oar.isPresent() && Constants.ACCESS_RIGHTS_COAR_MAP.containsKey(oar.get().getClassid())) {
			String code = Constants.ACCESS_RIGHTS_COAR_MAP.get(oar.get().getClassid());
			out
				.setBestAccessRight(

					BestAccessRight
						.newInstance(
							code,
							Constants.COAR_CODE_LABEL_MAP.get(code),
							Constants.COAR_ACCESS_RIGHT_SCHEMA));
		} else
			out.setBestAccessRight(null);
	}

	private static List<Author> mapAuthor(List<eu.dnetlib.dhp.schema.oaf.Author> input) {
		if (CollectionUtils.isEmpty(input))
			return null;
		List<Author> lista = input.stream().map(ResultMapper::getAuthor).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(lista))
			return null;
		else
			return lista;

	}

	private static void addTypeSpecificInformation(Result out, eu.dnetlib.dhp.schema.oaf.Result input,
		eu.dnetlib.dhp.schema.oaf.Qualifier ort) throws NoAvailableEntityTypeException {
		switch (ort.getClassid()) {
			case "publication":
				out.setContainer(mapContainer(((Publication) input).getJournal()));
				out.setType(ModelConstants.PUBLICATION_DEFAULT_RESULTTYPE.getClassname());
				break;
			case "dataset":
				Dataset id = (Dataset) input;
				out.setSize(mapFieldString(id.getSize()));
				out.setSize(mapFieldString(id.getVersion()));
				out.setGeoLocations(mapGeolocation(id.getGeolocation()));
				out.setType(ModelConstants.DATASET_DEFAULT_RESULTTYPE.getClassname());
				break;
			case "software":

				Software is = (Software) input;
				out.setCodeRepositoryUrl(mapFieldString(is.getCodeRepositoryUrl()));
				out.setDocumentationUrls(mapListString(is.getDocumentationUrl(), Field::getValue));
				out.setProgrammingLanguage(mapQualifierName(is.getProgrammingLanguage()));
				out.setType(ModelConstants.SOFTWARE_DEFAULT_RESULTTYPE.getClassname());
				break;
			case "other":

				OtherResearchProduct ir = (OtherResearchProduct) input;
				out.setContactGroups(mapListString(ir.getContactgroup(), Field::getValue));
				out.setContactPeople(mapListString(ir.getContactperson(), Field::getValue));
				out.setTools(mapListString(ir.getTool(), Field::getValue));

				out.setType(ModelConstants.ORP_DEFAULT_RESULTTYPE.getClassname());

				break;
			default:
				throw new NoAvailableEntityTypeException();
		}
	}

	private static List<GeoLocation> mapGeolocation(List<eu.dnetlib.dhp.schema.oaf.GeoLocation> in) {
		if (in == null || CollectionUtils.isEmpty(in))
			return null;
		List<GeoLocation> lista = in
			.stream()
			.filter(Objects::nonNull)
			.map(gli -> {
				GeoLocation gl = new GeoLocation();
				gl.setBox(gli.getBox());
				gl.setPlace(gli.getPlace());
				gl.setPoint(gli.getPoint());
				return gl;
			})
			.collect(Collectors.toList());

		return CollectionUtils.isEmpty(lista) ? null : lista;

	}

	private static Instance getGraphInstance(eu.dnetlib.dhp.schema.oaf.Instance i) {
		Instance instance = new Instance();

		setCommonValue(i, instance);

		return instance;

	}

	private static CommunityInstance getCommunityInstance(eu.dnetlib.dhp.schema.oaf.Instance i) {
		CommunityInstance instance = new CommunityInstance();

		setCommonValue(i, instance);

		if (Optional.ofNullable(i.getCollectedfrom()).isPresent() &&
			Optional.ofNullable(i.getCollectedfrom().getKey()).isPresent() &&
			StringUtils.isNotBlank(i.getCollectedfrom().getKey()))
			instance
				.setCollectedFrom(
					CfHbKeyValue
						.newInstance(
							getEntityId(i.getCollectedfrom().getKey(), ENTITY_ID_SEPARATOR),
							i.getCollectedfrom().getValue()));
		else
			i.setCollectedfrom(null);

		if (Optional.ofNullable(i.getHostedby()).isPresent() &&
			Optional.ofNullable(i.getHostedby().getKey()).isPresent() &&
			StringUtils.isNotBlank(i.getHostedby().getKey()))
			instance
				.setHostedBy(
					CfHbKeyValue
						.newInstance(
							getEntityId(i.getHostedby().getKey(), ENTITY_ID_SEPARATOR), i.getHostedby().getValue()));
		else
			i.setHostedby(null);
		return instance;

	}

	private static <I extends Instance> void setCommonValue(eu.dnetlib.dhp.schema.oaf.Instance i, I instance) {
		mapAccessRight(i, instance);
		instance
			.setPids(Utils.mapPid(i.getPid(), p -> ResultPid.newInstance(p.getQualifier().getClassid(), p.getValue())));
		instance
			.setAlternateIdentifiers(
				Utils
					.mapPid(
						i.getAlternateIdentifier(),
						p -> AlternateIdentifier.newInstance(p.getQualifier().getClassid(), p.getValue())));
		instance.setLicense(mapFieldString(i.getLicense()));
		instance.setPublicationDate(mapFieldString(i.getDateofacceptance()));
		instance.setRefereed(mapQualifierName(i.getRefereed()));
		instance.setType(mapQualifierName(i.getInstancetype()));
		instance.setUrls(Utils.mapListString(i.getUrl(), v -> v));
		mapAPC(i, instance);
	}

	private static <I extends Instance> void mapAccessRight(eu.dnetlib.dhp.schema.oaf.Instance i, I instance) {
		Optional<eu.dnetlib.dhp.schema.oaf.AccessRight> opAr = Optional.ofNullable(i.getAccessright());

		if (opAr.isPresent() && Constants.ACCESS_RIGHTS_COAR_MAP.containsKey(opAr.get().getClassid())) {
			String code = Constants.ACCESS_RIGHTS_COAR_MAP.get(opAr.get().getClassid());

			instance
				.setAccessRight(
					AccessRight
						.newInstance(
							code,
							Constants.COAR_CODE_LABEL_MAP.get(code),
							Constants.COAR_ACCESS_RIGHT_SCHEMA));

			instance.getAccessRight().setOpenAccessRoute(null);
			if (opAr.get().getOpenAccessRoute() != null) {
				switch (opAr.get().getOpenAccessRoute()) {
					case hybrid:
						instance.getAccessRight().setOpenAccessRoute(OpenAccessRoute.hybrid);
						break;
					case gold:
						instance.getAccessRight().setOpenAccessRoute(OpenAccessRoute.gold);
						break;
					case green:
						instance.getAccessRight().setOpenAccessRoute(OpenAccessRoute.green);
						break;
					case bronze:
						instance.getAccessRight().setOpenAccessRoute(OpenAccessRoute.bronze);
						break;

				}
			}

		} else
			instance.setAccessRight(null);
	}

	private static <I extends Instance> void mapAPC(eu.dnetlib.dhp.schema.oaf.Instance i, I instance) {
		Optional<Field<String>> oPca = Optional.ofNullable(i.getProcessingchargeamount());
		Optional<Field<String>> oPcc = Optional.ofNullable(i.getProcessingchargecurrency());
		if (oPca.isPresent() && oPcc.isPresent()) {
			Field<String> pca = oPca.get();
			Field<String> pcc = oPcc.get();
			if (!pca.getValue().trim().equals("") && !pcc.getValue().trim().equals("")) {
				APC apc = new APC();
				apc.setCurrency(oPcc.get().getValue());
				apc.setAmount(oPca.get().getValue());
				instance.setArticleProcessingCharge(apc);
			}

		}
	}

	private static List<Provenance> getUniqueProvenance(List<Provenance> provenance)
		throws NoAvailableEntityTypeException {
		Provenance iProv = new Provenance();

		Provenance hProv = new Provenance();
		Provenance lProv = new Provenance();

		for (Provenance p : provenance) {
			switch (p.getProvenance()) {
				case Constants.HARVESTED:
					hProv = getHighestTrust(hProv, p);
					break;
				case Constants.INFERRED:
					iProv = getHighestTrust(iProv, p);
					// To be removed as soon as the new beta run has been done
					// this fixex issue of not set trust during bulktagging
					if (StringUtils.isEmpty(iProv.getTrust())) {
						iProv.setTrust(Constants.DEFAULT_TRUST);
					}
					break;
				case Constants.USER_CLAIM:
					lProv = getHighestTrust(lProv, p);
					break;
				default:
					throw new NoAvailableEntityTypeException();
			}

		}

		return Arrays
			.asList(iProv, hProv, lProv)
			.stream()
			.filter(p -> !StringUtils.isEmpty(p.getProvenance()))
			.collect(Collectors.toList());

	}

	private static Provenance getHighestTrust(Provenance hProv, Provenance p) {
		if (StringUtils.isNoneEmpty(hProv.getTrust(), p.getTrust()))
			return hProv.getTrust().compareTo(p.getTrust()) > 0 ? hProv : p;

		return (StringUtils.isEmpty(p.getTrust()) && !StringUtils.isEmpty(hProv.getTrust())) ? hProv : p;

	}

	private static Subject getSubject(StructuredProperty s) {
		Subject subject = new Subject();
		subject.setSubject(SubjectSchemeValue.newInstance(s.getQualifier().getClassid(), s.getValue()));
		Optional<DataInfo> di = Optional.ofNullable(s.getDataInfo());
		if (di.isPresent()) {
			Provenance p = new Provenance();
			p.setProvenance(di.get().getProvenanceaction().getClassname());
			if (!s.getQualifier().getClassid().equalsIgnoreCase("fos") &&
				!s.getQualifier().getClassid().equalsIgnoreCase("sdg"))
				p.setTrust(di.get().getTrust());
			subject.setProvenance(p);
		}

		return subject;
	}

	private static Author getAuthor(eu.dnetlib.dhp.schema.oaf.Author oa) {
		Author a = new Author();
		a.setFullName(oa.getFullname());
		a.setName(oa.getName());
		a.setSurname(oa.getSurname());
		a.setRank(oa.getRank());

		a.setPid(null);
		Optional
			.ofNullable(oa.getPid())
			.ifPresent(structuredProperties -> a.setPid(getOrcid(structuredProperties)));

		return a;
	}

	private static AuthorPid getAuthorPid(StructuredProperty pid) {
		Optional<DataInfo> di = Optional.ofNullable(pid.getDataInfo());

		return di
			.map(
				dataInfo -> AuthorPid
					.newInstance(
						AuthorPidSchemeValue
							.newInstance(
								pid.getQualifier().getClassid(),
								pid.getValue()),
						Provenance
							.newInstance(
								dataInfo.getProvenanceaction().getClassname(),
								dataInfo.getTrust())))
			.orElseGet(
				() -> AuthorPid
					.newInstance(
						AuthorPidSchemeValue
							.newInstance(
								pid.getQualifier().getClassid(),
								pid.getValue())

					));
	}

	private static AuthorPid getOrcid(List<StructuredProperty> p) {
		List<StructuredProperty> pidList = p.stream().map(pid -> {
			if (pid.getQualifier().getClassid().equals(ModelConstants.ORCID) ||
				(pid.getQualifier().getClassid().equals(ModelConstants.ORCID_PENDING))) {
				return pid;
			}
			return null;
		}).filter(Objects::nonNull).collect(Collectors.toList());

		if (pidList.size() == 1) {
			return getAuthorPid(pidList.get(0));
		}

		List<StructuredProperty> orcid = pidList
			.stream()
			.filter(
				ap -> ap
					.getQualifier()
					.getClassid()
					.equals(ModelConstants.ORCID))
			.collect(Collectors.toList());
		if (orcid.size() == 1) {
			return getAuthorPid(orcid.get(0));
		}
		orcid = pidList
			.stream()
			.filter(
				ap -> ap
					.getQualifier()
					.getClassid()
					.equals(ModelConstants.ORCID_PENDING))
			.collect(Collectors.toList());
		if (orcid.size() == 1) {
			return getAuthorPid(orcid.get(0));
		}

		return null;
	}

}
