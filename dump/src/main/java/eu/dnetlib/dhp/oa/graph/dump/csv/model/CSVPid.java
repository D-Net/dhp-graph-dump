
package eu.dnetlib.dhp.oa.graph.dump.csv.model;

import java.io.Serializable;

import eu.dnetlib.dhp.oa.graph.dump.csv.Constants;

/**
 * @author miriam.baglioni
 * @Date 11/05/23
 */
public class CSVPid implements Serializable {

	private String id;
	private String result_id;
	private String pid;
	private String type;

	public String getResult_id() {
		return result_id;
	}

	public void setResult_id(String result_id) {
		this.result_id = Constants.addQuotes(result_id);
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = Constants.addQuotes(pid);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = Constants.addQuotes(type);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = Constants.addQuotes(id);
	}
}
