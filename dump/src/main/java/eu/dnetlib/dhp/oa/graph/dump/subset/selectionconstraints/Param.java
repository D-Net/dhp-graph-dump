
package eu.dnetlib.dhp.oa.graph.dump.subset.selectionconstraints;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.function.BiFunction;

import scala.xml.PrettyPrinter;

/**
 * @author miriam.baglioni
 * @Date 11/11/22
 */
public class Param extends HashMap<String, ArrayList<String>> implements Serializable {
	public Param() {
		super();
	}

	public void insert(String key, Object value) {
		if (value instanceof ArrayList)
			super.put(key, (ArrayList<String>) value);
		if (value instanceof String) {
			ArrayList<String> al = new ArrayList<>();
			al.add((String) value);
			super.put(key, al);
		}

	}

}
