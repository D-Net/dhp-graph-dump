
package eu.dnetlib.dhp.oa.graph.dump.complete;

import java.io.Serializable;

public class Constants implements Serializable {

	public static final String IS_HOSTED_BY = "isHostedBy";
	public static final String HOSTS = "hosts";

	public static final String RESULT_ENTITY = "product";
	public static final String DATASOURCE_ENTITY = "datasource";
	public static final String CONTEXT_ENTITY = "community";

	public static final String CONTEXT_NS_PREFIX = "community___";
	public static final String UNKNOWN = "UNKNOWN";

}
