
package eu.dnetlib.dhp.oa.graph.dump.csv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.oa.graph.dump.csv.model.CSVRELCommunityResult;
import eu.dnetlib.dhp.utils.DHPUtils;

/**
 * @author miriam.baglioni
 * @Date 11/05/23
 */
public class SelectResultAndDumpRelationTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static SparkSession spark;

	private static Path workingDir;

	private static final Logger log = LoggerFactory
		.getLogger(SelectResultAndDumpRelationTest.class);

	private static HashMap<String, String> map = new HashMap<>();

	@BeforeAll
	public static void beforeAll() throws IOException {
		workingDir = Files
			.createTempDirectory(SelectResultAndDumpRelationTest.class.getSimpleName());
		log.info("using work dir {}", workingDir);

		SparkConf conf = new SparkConf();
		conf.setAppName(SelectResultAndDumpRelationTest.class.getSimpleName());

		conf.setMaster("local[*]");
		conf.set("spark.driver.host", "localhost");
		conf.set("hive.metastore.local", "true");
		conf.set("spark.ui.enabled", "false");
		conf.set("spark.sql.warehouse.dir", workingDir.toString());
		conf.set("hive.metastore.warehouse.dir", workingDir.resolve("warehouse").toString());

		spark = SparkSession
			.builder()
			.appName(SelectResultAndDumpRelationTest.class.getSimpleName())
			.config(conf)
			.getOrCreate();
	}

	@AfterAll
	public static void afterAll() throws IOException {
		FileUtils.deleteDirectory(workingDir.toFile());
		spark.stop();
	}

	@Test
	public void test1() throws Exception {

		final String sourcePath = getClass()
			.getResource("/eu/dnetlib/dhp/oa/graph/dump/csv/input/")
			.getPath();

		SparkSelectResultsAndDumpRelations.main(new String[] {
			"-isSparkSessionManaged", Boolean.FALSE.toString(),
			"-outputPath", workingDir.toString() + "/output",
			"-workingPath", workingDir.toString() + "/working",
			"-communities", "enermaps;dh-ch",
			"-sourcePath", sourcePath
		});

		final JavaSparkContext sc = JavaSparkContext.fromSparkContext(spark.sparkContext());

		Assertions.assertEquals(2, sc.textFile(workingDir.toString() + "/working/communityResultIds").count());

		Assertions
			.assertEquals(
				1, sc
					.textFile(workingDir.toString() + "/working/communityResultIds")
					.filter(v -> v.equals("50|DansKnawCris::0224aae28af558f21768dbc6439c7a95"))
					.count());

		Assertions
			.assertEquals(
				1, sc
					.textFile(workingDir.toString() + "/working/communityResultIds")
					.filter(v -> v.equals("50|DansKnawCris::20c414a3b1c742d5dd3851f1b67df2d9"))
					.count());

		// verify that the association is correct with the communityid and result id
		spark
			.read()
			.option("header", "true")
			.option("delimiter", Constants.SEP)
			.csv(workingDir.toString() + "/output/result_community")
			.createOrReplaceTempView("result_community");

		Assertions.assertEquals(3, spark.sql("SELECT * FROM result_community").count());

		Assertions
			.assertEquals(
				1, spark
					.sql(
						"SELECT * " +
							"FROM result_community " +
							"WHERE community_id = '" + DHPUtils.md5("dh-ch") + "'")
					.count());

		Assertions
			.assertEquals(
				1, spark
					.sql(
						"SELECT * " +
							"FROM result_community" +
							" WHERE result_id = '50|DansKnawCris::0224aae28af558f21768dbc6439c7a95' " +
							"AND community_id = '" + DHPUtils.md5("dh-ch") + "'")
					.count());

		Assertions
			.assertEquals(
				2, spark
					.sql(
						"SELECT * " +
							"FROM result_community " +
							"WHERE community_id = '" + DHPUtils.md5("enermaps") + "'")
					.count());
		Assertions
			.assertEquals(
				1, spark
					.sql(
						"SELECT * " +
							"FROM result_community " +
							"WHERE result_id = '50|DansKnawCris::20c414a3b1c742d5dd3851f1b67df2d9' " +
							"AND community_id = '" + DHPUtils.md5("enermaps") + "'")
					.count());
		Assertions
			.assertEquals(
				1, spark
					.sql(
						"SELECT * " +
							"FROM result_community " +
							"WHERE result_id = '50|DansKnawCris::0224aae28af558f21768dbc6439c7a95' " +
							"AND community_id = '" + DHPUtils.md5("enermaps") + "'")
					.count());

		Assertions.assertEquals(3, spark.read().textFile(workingDir.toString() + "/working/resultIds").count());

		Assertions
			.assertEquals(
				1, sc
					.textFile(workingDir.toString() + "/working/resultIds")
					.filter(v -> v.equals("50|DansKnawCris::0224aae28af558f21768dbc6439c7a95"))
					.count());

		Assertions
			.assertEquals(
				1, sc
					.textFile(workingDir.toString() + "/working/resultIds")
					.filter(v -> v.equals("50|DansKnawCris::20c414a3b1c742d5dd3851f1b67df2d9"))
					.count());

		Assertions
			.assertEquals(
				1, sc
					.textFile(workingDir.toString() + "/working/resultIds")
					.filter(v -> v.equals("50|DansKnawCris::26780065282e607306372abd0d808245"))
					.count());

		spark
			.read()
			.option("header", "true")
			.option("delimiter", Constants.SEP)
			.csv(workingDir.toString() + "/output/relation")
			.createOrReplaceTempView("relation");

		Assertions.assertEquals(2, spark.sql("SELECT * FROM relation").count());

		Assertions
			.assertEquals(
				1, spark
					.sql(
						"SELECT * FROM relation WHERE id = '" +
							DHPUtils
								.md5(
									("50|DansKnawCris::20c414a3b1c742d5dd3851f1b67df2d9cites50|DansKnawCris::26780065282e607306372abd0d808245"))
							+ "'")
					.count());

		Assertions
			.assertEquals(
				1, spark
					.sql(
						"SELECT * FROM relation WHERE id = '" +
							DHPUtils
								.md5(
									("50|DansKnawCris::20c414a3b1c742d5dd3851f1b67df2d9cites50|DansKnawCris::0224aae28af558f21768dbc6439c7a95"))
							+ "'")
					.count());

	}

}
