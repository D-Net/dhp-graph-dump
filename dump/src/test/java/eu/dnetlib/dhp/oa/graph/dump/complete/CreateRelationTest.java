
package eu.dnetlib.dhp.oa.graph.dump.complete;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.dnetlib.dhp.communityapi.QueryCommunityAPI;
import eu.dnetlib.dhp.oa.graph.dump.UtilCommunityAPI;
import eu.dnetlib.dhp.oa.graph.dump.subset.MasterDuplicate;
import eu.dnetlib.dhp.oa.model.graph.Relation;
import eu.dnetlib.dhp.schema.common.ModelSupport;
import eu.dnetlib.dhp.schema.oaf.Datasource;
import eu.dnetlib.dhp.schema.oaf.Project;
import eu.dnetlib.dhp.utils.DHPUtils;

class CreateRelationTest {

	String contextInfo1 = "{\"id\":\"eut\",\"description\":null,\"type\":null,\"zenodocommunity\":null,\"name\":null,\"projectList\":[],\"datasourceList\":[\"opendoar____::39e4973ba3321b80f37d9b55f63ed8b8\",\"opendoar____::cda72177eba360ff16b7f836e2754370\",\"opendoar____::71f6278d140af599e06ad9bf1ba03cb0\",\"opendoar____::f5c59267dae7d123f54b741a76f28f84\",\"opendoar____::532a082cc38eaffa923d73bce41b4061\",\"opendoar____::00a03ec6533ca7f5c644d198d815329c\",\"opendoar____::33a854e247155d590883b93bca53848a\"],\"subject\":null}";
	String contextInfo2 = "{\"id\":\"knowmad\",\"description\":null,\"type\":null,\"zenodocommunity\":null,\"name\":null,\"projectList\":[],\"datasourceList\":[\"issn___print::81db6614a3fa07becf706193d4754f30\",\"doajarticles::71eae002613af3569ea42b7093eefdbd\",\"doajarticles::daf53452e1199952d6c95ee7d0088d35\",\"issn___print::ff1a02f622ff006edc668e55fc865ae8\",\"doajarticles::14c007581eb12d843050ebf0493f2254\",\"issn__online::14c007581eb12d843050ebf0493f2254\",\"doajarticles::19a7308c7287b3784aacdb1979141c50\",\"doajarticles::c28985d2363f1ed20048dfd682ee14be\",\"issn__online::c28985d2363f1ed20048dfd682ee14be\",\"doajarticles::81db6614a3fa07becf706193d4754f30\",\"doajarticles::588fc0f98218e9f29a0dabe1182851c4\",\"doajarticles::5ccbe475390dfa642a356c7fd678b70a\",\"doajarticles::cf63b988539f8d28ed366220691f751b\",\"doajarticles::c03353a080ac66c37a1f6aa2de05ca63\",\"issn__online::efec4cc1b1143f69d7d0954a2e9b18b6\",\"doajarticles::25ddd7bb737fc8b027b455f6712111a0\",\"doajarticles::ca2e3cc507ff73298333ed53177e9916\",\"doajarticles::220ab67f748963b6932600585452ad83\",\"doajarticles::b718013941a35a86b17b9b57aca9d260\",\"doajarticles::934e4b68deaeee0781f18a6a6e4fd906\",\"doajarticles::43587737046be23d692860e3212d6966\",\"doajarticles::23a27fb45b9af1f2b57632b7ceb98448\",\"doajarticles::9d32313b109db4f024eb1c802ad4e353\",\"fairsharing_::b4944963b5c83d545c3d3022bcf03282\",\"doajarticles::b5a2cadc830f94ceb2f8035369cf66a1\",\"doajarticles::8d64614210c51d4bc9e1470bbe1b5607\",\"doajarticles::e258604e7e1132683570d4178e9bec8f\",\"doajarticles::260c4ba804c4c08842ad39a6e8fd2a97\",\"doajarticles::8c9f0a0c20cb062a0ab7823af8262d0e\",\"issn__online::ef2866d8e4561162a1b115c24ebd7887\",\"doajarticles::de16206a48e58e344ef049670a1a0134\",\"doajarticles::96621c5056169f56ff3131ea90aa07c9\",\"issn__online::4558a062701b28ec0b4a6b2f0fbe8d09\",\"issn___print::5571b62bd2e69e2311990fceb7fe2e3a\",\"doajarticles::c61ecda4a5a75b94a28b1c782008e64c\",\"doajarticles::fd825a036d04fcdf0ab805d87e16d1de\",\"issn___print::61d35d3f7e7f635fa9818eb5c5724833\",\"doajarticles::685e5ac5fb55c74ee8a2b266923e1f1d\",\"doajarticles::b99614718f7a6545b1eed9b68d3441de\",\"doajarticles::7d2d2e0095bf1ec755bf3e39d31a5bcb\",\"doajarticles::6124a1bc19d8f538c1bb45da8fda914b\",\"issn___print::6f2e006697b7a4f9d6e2e4d61d9e87c2\",\"issn___print::24d6c4cc60c9446680a4bf6464f232cc\",\"issn___print::0482ed6012f16fa029dd0434b644cb90\",\"issn___print::74d68388e8ad50f80b754f5036c80fcf\",\"issn__online::22b586e63e4962054a332a538e5d2b71\",\"issn___print::4bceffb60baed7f1e3f25b171b8fcf63\",\"issn__online::4074709fc410b3be61ea1769634ae8ff\",\"issn__online::d787423124beb54d12945b8fb6effe17\",\"issn__online::92d6a58173da7e479557acb8701da9dc\",\"issn___print::36813e86616b329b06101afab0f2d115\",\"issn___print::60f102cf59ddbfc2297dbfd2efe1c006\",\"issn__online::e2bedb8ffd0dd9a05af59c2651eb4200\",\"issn___print::3a5263873041ce993d856c3a08b87e66\",\"issn___print::c0118216b90b4ec13c4344c302eb1cc2\",\"issn___print::36cb3b21af46a23327d120d848ac5256\",\"issn___print::f6528b255fa5b915efddf5bdd4b12bef\",\"issn___print::50ae07b765e6c893d93701b63dc885eb\",\"issn___print::1efd4ac736f64f83bc537339bf112882\",\"issn___print::f46704a2eb1338c3524391c7fcdc5f50\",\"issn__online::6e823cb8d73afda60e12a0ce7ec65fe3\",\"doajarticles::df6746f5d17a6c27cf1033e2888b934d\",\"issn___print::8a8a700ac926378b330e57d7faf5948e\",\"issn___print::6fbc352ab2267a17490b922033c2ce8c\",\"issn___print::692e57ca190e52967683bca19dcc6200\",\"issn___print::4f657189126a3c51cf8f2d58410d09b8\",\"issn___print::2fd71afb352b4ec1ee58b2bfa4d536da\",\"doajarticles::e91e4788bfb04277b61416fd184416b2\",\"issn__online::3097c43f11470b27e74f040cf267eedf\",\"doajarticles::942ba9d488531f3a60633b716db0b05c\",\"doajarticles::1aefeb2e98d29b17473e3f4dbcc5000e\",\"issn___print::223b857892c5de7247b3f906e1e1b96a\",\"doajarticles::b64f9c27d2c8899df904930cd6d2316a\"],\"subject\":null}";
	String contextInfo3 = "{\"id\":\"ni\",\"description\":null,\"type\":null,\"zenodocommunity\":null,\"name\":null,\"projectList\":[\"nih_________::412379d1e2683a9571405e6621d3cdeb\",\"nsf_________::1e685a3d3ca1d62192bb51d42d44c6d1\",\"nsf_________::8ff441b675f2c62061f15210b05c1584\",\"nih_________::2d8bd32ed8cb2f0b0d4508df61d494dd\",\"nsf_________::e71b0a0f6a347d76816adb29b322441c\",\"nih_________::c27d60d28c9bc5cf17dd7ae7ad2d4ab4\",\"nih_________::b28919975c85cfee114e442c30c918c6\",\"wt__________::d709ac8e1ed393d036c895d239d825a4\"],\"datasourceList\":[\"re3data_____::5b9bf9171d92df854cf3c520692e9122\",\"doajarticles::c7d3de67dc77af72f6747157441252ec\",\"re3data_____::8515794670370f49c1d176c399c714f5\",\"doajarticles::d640648c84b10d425f96f11c3de468f3\",\"doajarticles::0c0e74daa5d95504eade9c81ebbd5b8a\",\"rest________::fb1a3d4523c95e63496e3bc7ba36244b\",\"opendoar____::7e7757b1e12abcb736ab9a754ffb617a\"],\"subject\":null}";

	@Test
	void test1() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		List<ContextInfo> cInfoList = new ArrayList<>();
		final Consumer<ContextInfo> consumer = ci -> cInfoList.add(ci);

		consumer.accept(mapper.readValue(contextInfo1, ContextInfo.class));
		consumer.accept(mapper.readValue(contextInfo2, ContextInfo.class));
		consumer.accept(mapper.readValue(contextInfo3, ContextInfo.class));

		List<Relation> rList = new ArrayList<>();

		cInfoList.forEach(cInfo -> Process.getRelation(cInfo).forEach(rList::add));

		Assertions.assertEquals(190, rList.size());

		Assertions
			.assertTrue(
				rList
					.stream()
					.map(r -> r.getSource())
					.collect(Collectors.toSet())
					.contains(
						String
							.format(
								"%s::%s",
								Constants.CONTEXT_NS_PREFIX,
								DHPUtils.md5("eut"))));

		Assertions
			.assertEquals(
				7,
				rList
					.stream()
					.filter(
						r -> r
							.getSource()

							.equals(
								String
									.format(
										"%s::%s",
										Constants.CONTEXT_NS_PREFIX,
										DHPUtils.md5("eut"))))
					.collect(Collectors.toList())
					.size());

		Assertions
			.assertEquals(
				7,
				rList
					.stream()
					.filter(
						r -> r
							.getTarget()

							.equals(
								String
									.format(
										"%s::%s",
										Constants.CONTEXT_NS_PREFIX,
										DHPUtils.md5("eut"))))
					.collect(Collectors.toList())
					.size());

		Set<String> tmp = rList
			.stream()
			.filter(
				r -> r
					.getSource()

					.equals(
						String
							.format(
								"%s::%s",
								Constants.CONTEXT_NS_PREFIX,
								DHPUtils.md5("eut"))))
			.map(r -> r.getTarget())
			.collect(Collectors.toSet());

		Assertions
			.assertTrue(
				tmp.contains("opendoar____::532a082cc38eaffa923d73bce41b4061") &&
					tmp.contains("opendoar____::00a03ec6533ca7f5c644d198d815329c") &&
					tmp.contains("opendoar____::33a854e247155d590883b93bca53848a") &&
					tmp.contains("opendoar____::71f6278d140af599e06ad9bf1ba03cb0") &&
					tmp.contains("opendoar____::f5c59267dae7d123f54b741a76f28f84") &&
					tmp.contains("opendoar____::cda72177eba360ff16b7f836e2754370") &&
					tmp.contains("opendoar____::39e4973ba3321b80f37d9b55f63ed8b8"));

		Assertions
			.assertTrue(
				rList
					.stream()
					.filter(
						r -> r
							.getSource()

							.equals(
								String
									.format(
										"%s::%s",
										Constants.CONTEXT_NS_PREFIX,
										DHPUtils.md5("eut"))))
					.map(r -> r.getTargetType())
					.collect(Collectors.toSet())
					.stream()
					.allMatch(t -> t.equals("datasource")));

		Assertions
			.assertEquals(
				15,
				rList
					.stream()
					.filter(
						r -> r
							.getTarget()

							.equals(
								String
									.format(
										"%s::%s",
										Constants.CONTEXT_NS_PREFIX,
										DHPUtils.md5("ni"))))
					.collect(Collectors.toList())
					.size());

		Assertions
			.assertEquals(
				7, rList
					.stream()
					.filter(
						r -> r
							.getSource()
							.equals(
								String
									.format(
										"%s::%s",
										Constants.CONTEXT_NS_PREFIX,
										DHPUtils.md5("ni")))
							&&
							r.getTargetType().equals("datasource"))
					.count());

		Assertions
			.assertEquals(
				8, rList
					.stream()
					.filter(
						r -> r
							.getSource()
							.equals(
								String
									.format(
										"%s::%s",
										Constants.CONTEXT_NS_PREFIX,
										DHPUtils.md5("ni")))
							&&
							r.getTargetType().equals("project"))
					.count());
	}

}
