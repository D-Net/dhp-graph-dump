
package eu.dnetlib.dhp.oa.graph.dump.complete;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.oa.graph.dump.exceptions.NoAvailableEntityTypeException;
import eu.dnetlib.dhp.oa.model.Indicator;
import eu.dnetlib.dhp.schema.oaf.Datasource;
import eu.dnetlib.dhp.schema.oaf.Organization;
import eu.dnetlib.dhp.schema.oaf.Project;

public class DumpOrganizationProjectDatasourceTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static SparkSession spark;

	private static Path workingDir;

	private static final Logger log = LoggerFactory
		.getLogger(DumpOrganizationProjectDatasourceTest.class);

	private static final HashMap<String, String> map = new HashMap<>();

	@BeforeAll
	public static void beforeAll() throws IOException {
		workingDir = Files
			.createTempDirectory(DumpOrganizationProjectDatasourceTest.class.getSimpleName());
		log.info("using work dir {}", workingDir);

		SparkConf conf = new SparkConf();
		conf.setAppName(DumpOrganizationProjectDatasourceTest.class.getSimpleName());

		conf.setMaster("local[*]");
		conf.set("spark.driver.host", "localhost");
		conf.set("hive.metastore.local", "true");
		conf.set("spark.ui.enabled", "false");
		conf.set("spark.sql.warehouse.dir", workingDir.toString());
		conf.set("hive.metastore.warehouse.dir", workingDir.resolve("warehouse").toString());

		spark = SparkSession
			.builder()
			.appName(DumpOrganizationProjectDatasourceTest.class.getSimpleName())
			.config(conf)
			.getOrCreate();
	}

	@AfterAll
	public static void afterAll() throws IOException {
		FileUtils.deleteDirectory(workingDir.toFile());
		spark.stop();
	}

	@Test
	public void dumpOrganizationTest() throws Exception {

		final String sourcePath = getClass()
			.getResource("/eu/dnetlib/dhp/oa/graph/dump/complete/organization")
			.getPath();

		SparkDumpEntitiesJob
			.main(
				new String[] {
					"-isSparkSessionManaged", Boolean.FALSE.toString(),
					"-sourcePath", sourcePath,
					"-resultTableName", "eu.dnetlib.dhp.schema.oaf.Organization",
					"-outputPath", workingDir.toString() + "/dump"

				});

		final JavaSparkContext sc = JavaSparkContext.fromSparkContext(spark.sparkContext());

		JavaRDD<eu.dnetlib.dhp.oa.model.graph.Organization> tmp = sc
			.textFile(workingDir.toString() + "/dump")
			.map(item -> OBJECT_MAPPER.readValue(item, eu.dnetlib.dhp.oa.model.graph.Organization.class));

		org.apache.spark.sql.Dataset<eu.dnetlib.dhp.oa.model.graph.Organization> verificationDataset = spark
			.createDataset(tmp.rdd(), Encoders.bean(eu.dnetlib.dhp.oa.model.graph.Organization.class));

		Assertions.assertEquals(15, verificationDataset.count());

		// TODO write significant assertions
//		verificationDataset
//			.foreach(
//				(ForeachFunction<eu.dnetlib.dhp.oa.model.graph.Organization>) o -> System.out
//					.println(OBJECT_MAPPER.writeValueAsString(o)));

	}

	@Test
	public void dumpProjectTest() throws Exception {

		final String sourcePath = getClass()
			.getResource("/eu/dnetlib/dhp/oa/graph/dump/complete/project")
			.getPath();

		SparkDumpEntitiesJob
			.main(
				new String[] {
					"-isSparkSessionManaged", Boolean.FALSE.toString(),
					"-sourcePath", sourcePath,
					"-resultTableName", "eu.dnetlib.dhp.schema.oaf.Project",
					"-outputPath", workingDir.toString() + "/dump"

				});

		final JavaSparkContext sc = JavaSparkContext.fromSparkContext(spark.sparkContext());

		JavaRDD<eu.dnetlib.dhp.oa.model.graph.Project> tmp = sc
			.textFile(workingDir.toString() + "/dump")
			.map(item -> OBJECT_MAPPER.readValue(item, eu.dnetlib.dhp.oa.model.graph.Project.class));

		org.apache.spark.sql.Dataset<eu.dnetlib.dhp.oa.model.graph.Project> verificationDataset = spark
			.createDataset(tmp.rdd(), Encoders.bean(eu.dnetlib.dhp.oa.model.graph.Project.class));

		Assertions.assertEquals(12, verificationDataset.count());

		Assertions.assertEquals(10, verificationDataset.filter("indicators is NULL").count());
		Assertions.assertEquals(2, verificationDataset.filter("indicators is not NULL").count());
		Assertions
			.assertEquals(
				1,
				verificationDataset
					.filter("indicators is not NULL AND id == 'aka_________::01bb7b48e29d732a1c7bc5150b9195c4'")
					.count());
		Assertions
			.assertEquals(
				1,
				verificationDataset
					.filter("indicators is not NULL AND id == 'aka_________::9d1af21dbd0f5bc719f71553d19a6b3a'")
					.count());

//		eu.dnetlib.dhp.oa.model.graph.Project p = tmp
//			.filter(pr -> pr.getId().equals("aka_________::01bb7b48e29d732a1c7bc5150b9195c4"))
//			.first();
//		Assertions.assertEquals("2019", p.getIndicators().getUsageCounts().getDownloads());
//		Assertions.assertEquals("1804", p.getIndicators().getUsageCounts().getViews());
//		Assertions.assertNull(p.getIndicators().getImpactMeasures());

//		p = tmp.filter(pr -> pr.getId().equals("aka_________::9d1af21dbd0f5bc719f71553d19a6b3a")).first();
//		Assertions.assertEquals("139", p.getIndicators().getUsageCounts().getDownloads());
//		Assertions.assertEquals("53", p.getIndicators().getUsageCounts().getViews());
//		Assertions.assertNull(p.getIndicators().getImpactMeasures());
		// TODO write significant assertions
//		verificationDataset
//			.foreach(
//				(ForeachFunction<eu.dnetlib.dhp.oa.model.graph.Project>) o -> System.out
//					.println(OBJECT_MAPPER.writeValueAsString(o)));

	}

	@Test
	public void dumpDatasourceTest() throws Exception {
		final String sourcePath = getClass()
			.getResource("/eu/dnetlib/dhp/oa/graph/dump/complete/datasource")
			.getPath();

		SparkDumpEntitiesJob
			.main(
				new String[] {
					"-isSparkSessionManaged", Boolean.FALSE.toString(),
					"-sourcePath", sourcePath,
					"-resultTableName", "eu.dnetlib.dhp.schema.oaf.Datasource",
					"-outputPath", workingDir.toString() + "/dump"

				});

		final JavaSparkContext sc = JavaSparkContext.fromSparkContext(spark.sparkContext());

		JavaRDD<eu.dnetlib.dhp.oa.model.graph.Datasource> tmp = sc
			.textFile(workingDir.toString() + "/dump")
			.map(item -> OBJECT_MAPPER.readValue(item, eu.dnetlib.dhp.oa.model.graph.Datasource.class));

		org.apache.spark.sql.Dataset<eu.dnetlib.dhp.oa.model.graph.Datasource> verificationDataset = spark
			.createDataset(tmp.rdd(), Encoders.bean(eu.dnetlib.dhp.oa.model.graph.Datasource.class));

		Assertions.assertEquals(5, verificationDataset.count());

		Assertions.assertEquals(3, verificationDataset.filter("indicators is NULL").count());
		Assertions.assertEquals(2, verificationDataset.filter("indicators is not NULL").count());
		Assertions
			.assertEquals(
				1,
				verificationDataset
					.filter("indicators is not NULL AND id == 'doajarticles::1fa6859d71faa77b32d82f278c6ed1df'")
					.count());
		Assertions
			.assertEquals(
				1,
				verificationDataset
					.filter("indicators is not NULL AND id == 'doajarticles::9c4b678901e5276d9e3addee566816af'")
					.count());

//		eu.dnetlib.dhp.oa.model.graph.Datasource p = tmp
//			.filter(pr -> pr.getId().equals("doajarticles::1fa6859d71faa77b32d82f278c6ed1df"))
//			.first();
//		Assertions.assertEquals("47542", p.getIndicators().getUsageCounts().getDownloads());
//		Assertions.assertEquals("36485", p.getIndicators().getUsageCounts().getViews());
//		Assertions.assertNull(p.getIndicators().getImpactMeasures());
//
//		p = tmp.filter(pr -> pr.getId().equals("doajarticles::9c4b678901e5276d9e3addee566816af")).first();
//		Assertions.assertEquals("981357", p.getIndicators().getUsageCounts().getDownloads());
//		Assertions.assertEquals("646539", p.getIndicators().getUsageCounts().getViews());
//		Assertions.assertNull(p.getIndicators().getImpactMeasures());

		// TODO write significant assertions
//		verificationDataset
//			.foreach(
//				(ForeachFunction<eu.dnetlib.dhp.oa.model.graph.Datasource>) o -> System.out
//					.println(OBJECT_MAPPER.writeValueAsString(o)));
	}

	@Test
	public void dumpDatasourceNotDumpedTest() throws Exception {
		final String sourcePath = getClass()
			.getResource("/eu/dnetlib/dhp/oa/graph/dump/complete/datasourcenotdumped")
			.getPath();

		SparkDumpEntitiesJob
			.main(
				new String[] {
					"-isSparkSessionManaged", Boolean.FALSE.toString(),
					"-sourcePath", sourcePath,
					"-resultTableName", "eu.dnetlib.dhp.schema.oaf.Datasource",
					"-outputPath", workingDir.toString() + "/dump"

				});

		final JavaSparkContext sc = JavaSparkContext.fromSparkContext(spark.sparkContext());

		JavaRDD<eu.dnetlib.dhp.oa.model.graph.Datasource> tmp = sc
			.textFile(workingDir.toString() + "/dump")
			.map(item -> OBJECT_MAPPER.readValue(item, eu.dnetlib.dhp.oa.model.graph.Datasource.class));

		org.apache.spark.sql.Dataset<eu.dnetlib.dhp.oa.model.graph.Datasource> verificationDataset = spark
			.createDataset(tmp.rdd(), Encoders.bean(eu.dnetlib.dhp.oa.model.graph.Datasource.class));

		Assertions.assertEquals(1, verificationDataset.count());

		// TODO write significant assertions
//		verificationDataset
//			.foreach(
//				(ForeachFunction<eu.dnetlib.dhp.oa.model.graph.Datasource>) o -> System.out
//					.println(OBJECT_MAPPER.writeValueAsString(o)));
	}

}
