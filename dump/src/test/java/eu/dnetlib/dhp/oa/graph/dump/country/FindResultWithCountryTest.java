
package eu.dnetlib.dhp.oa.graph.dump.country;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.PublicKey;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.oa.graph.dump.Utils;
import eu.dnetlib.dhp.oa.model.graph.Relation;
import eu.dnetlib.dhp.schema.oaf.Publication;

/**
 * @author miriam.baglioni
 * @Date 02/05/23
 */
public class FindResultWithCountryTest {
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static SparkSession spark;

	private static Path workingDir;

	private static final Logger log = LoggerFactory
		.getLogger(FindResultWithCountryTest.class);

	private static final HashMap<String, String> map = new HashMap<>();

	@BeforeAll
	public static void beforeAll() throws IOException {
		workingDir = Files
			.createTempDirectory(FindResultWithCountryTest.class.getSimpleName());
		log.info("using work dir {}", workingDir);

		SparkConf conf = new SparkConf();
		conf.setAppName(FindResultWithCountryTest.class.getSimpleName());

		conf.setMaster("local[*]");
		conf.set("spark.driver.host", "localhost");
		conf.set("hive.metastore.local", "true");
		conf.set("spark.ui.enabled", "false");
		conf.set("spark.sql.warehouse.dir", workingDir.toString());
		conf.set("hive.metastore.warehouse.dir", workingDir.resolve("warehouse").toString());

		spark = SparkSession
			.builder()
			.appName(FindResultWithCountryTest.class.getSimpleName())
			.config(conf)
			.getOrCreate();
	}

	@AfterAll
	public static void afterAll() throws IOException {
		FileUtils.deleteDirectory(workingDir.toFile());
		spark.stop();
	}

	@Test
	void test1() throws Exception {

		final String sourcePath = getClass()
			.getResource("/eu/dnetlib/dhp/oa/graph/dump/country/graph/publication")
			.getPath();

		spark
			.read()
			.textFile(
				getClass()
					.getResource("/eu/dnetlib/dhp/oa/graph/dump/country/resultWithCountry")
					.getPath())
			.write()
			.text(workingDir.toString() + "/resWithCountry");

		SparkFindResultWithCountry.main(new String[] {
			"-isSparkSessionManaged", Boolean.FALSE.toString(),
			"-outputPath", workingDir.toString() + "/out",
			"-sourcePath", sourcePath,
			"-resultType", "publication",
			"-resultTableName", "eu.dnetlib.dhp.schema.oaf.Publication",
			"-resultWithCountry", workingDir.toString() + "/resWithCountry"
		});

		final JavaSparkContext sc = JavaSparkContext.fromSparkContext(spark.sparkContext());
		JavaRDD<Publication> tmp = sc
			.textFile(workingDir.toString() + "/out/original/publication")
			.map(item -> OBJECT_MAPPER.readValue(item, Publication.class));

		Assertions.assertEquals(2, tmp.count());

		Assertions
			.assertTrue(
				tmp.map(p -> p.getId()).collect().contains("50|06cdd3ff4700::136eb030ccb020e170df9627fa1a70af"));
		Assertions
			.assertTrue(
				tmp.map(p -> p.getId()).collect().contains("50|06cdd3ff4700::136eb030ccb020e170df9627fa1a70ag"));

	}
}
