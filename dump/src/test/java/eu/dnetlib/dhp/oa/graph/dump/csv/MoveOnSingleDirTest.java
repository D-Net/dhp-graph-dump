
package eu.dnetlib.dhp.oa.graph.dump.csv;

import static org.apache.commons.lang3.StringUtils.split;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.schema.common.ModelConstants;

/**
 * @author miriam.baglioni
 * @Date 25/05/23
 */
public class MoveOnSingleDirTest {
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static SparkSession spark;

	private static Path workingDir;

	private static final Logger log = LoggerFactory
		.getLogger(MoveOnSingleDirTest.class);

	private static HashMap<String, String> map = new HashMap<>();

	@BeforeAll
	public static void beforeAll() throws IOException {
		workingDir = Files
			.createTempDirectory(MoveOnSingleDirTest.class.getSimpleName());
		log.info("using work dir {}", workingDir);

		SparkConf conf = new SparkConf();
		conf.setAppName(MoveOnSingleDirTest.class.getSimpleName());

		conf.setMaster("local[*]");
		conf.set("spark.driver.host", "localhost");
		conf.set("hive.metastore.local", "true");
		conf.set("spark.ui.enabled", "false");
		conf.set("spark.sql.warehouse.dir", workingDir.toString());
		conf.set("hive.metastore.warehouse.dir", workingDir.resolve("warehouse").toString());

		spark = SparkSession
			.builder()
			.appName(MoveOnSingleDirTest.class.getSimpleName())
			.config(conf)
			.getOrCreate();
	}

	@AfterAll
	public static void afterAll() throws IOException {
		FileUtils.deleteDirectory(workingDir.toFile());
		spark.stop();
	}

	@Test
	public void testDMoveSingleDir() throws Exception {

		final String workingPath = getClass()
			.getResource("/eu/dnetlib/dhp/oa/graph/dump/csv/working")
			.getPath();

		spark
			.read()
			.text(
				getClass()
					.getResource("/eu/dnetlib/dhp/oa/graph/dump/csv/working/resultIds")
					.getPath())
			.write()
			.text(workingDir.toString() + "/working/resultIds/");

		SparkMoveOnSigleDir.main(new String[] {
			"-isSparkSessionManaged", Boolean.FALSE.toString(),
			"-outputPath", workingDir.toString() + "/output",
			"-workingPath", workingPath
		});

		Dataset<Row> tmp = spark
			.read()
			.option("header", "true")
			.option("delimiter", Constants.SEP)
			.csv(workingDir.toString() + "/output/result");

		Assertions.assertEquals(22, tmp.count());
		Assertions.assertEquals(12, tmp.filter("type == 'dataset'").count());
		Assertions.assertEquals(4, tmp.filter("type == 'other'").count());
		Assertions.assertEquals(5, tmp.filter("type == 'publication'").count());
		Assertions.assertEquals(1, tmp.filter("type == 'software'").count());

		tmp.filter("type == 'publication'").show(false);

		Assertions
			.assertEquals(
				8, spark
					.read()
					.option("header", "true")
					.option("delimiter", Constants.SEP)
					.csv(workingDir.toString() + "/output/author")
					.count());

	}
}
