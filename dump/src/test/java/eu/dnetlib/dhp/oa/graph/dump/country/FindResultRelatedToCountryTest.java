
package eu.dnetlib.dhp.oa.graph.dump.country;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.neethi.Assertion;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author miriam.baglioni
 * @Date 02/05/23
 */
public class FindResultRelatedToCountryTest {

	private static SparkSession spark;

	private static Path workingDir;

	private static final Logger log = LoggerFactory
		.getLogger(FindResultRelatedToCountryTest.class);

	private static final HashMap<String, String> map = new HashMap<>();

	@BeforeAll
	public static void beforeAll() throws IOException {
		workingDir = Files
			.createTempDirectory(FindResultRelatedToCountryTest.class.getSimpleName());
		log.info("using work dir {}", workingDir);

		SparkConf conf = new SparkConf();
		conf.setAppName(FindResultRelatedToCountryTest.class.getSimpleName());

		conf.setMaster("local[*]");
		conf.set("spark.driver.host", "localhost");
		conf.set("hive.metastore.local", "true");
		conf.set("spark.ui.enabled", "false");
		conf.set("spark.sql.warehouse.dir", workingDir.toString());
		conf.set("hive.metastore.warehouse.dir", workingDir.resolve("warehouse").toString());

		spark = SparkSession
			.builder()
			.appName(FindResultRelatedToCountryTest.class.getSimpleName())
			.config(conf)
			.getOrCreate();
	}

	@AfterAll
	public static void afterAll() throws IOException {
		FileUtils.deleteDirectory(workingDir.toFile());
		spark.stop();
	}

	@Test
	void test1() throws Exception {

		final String sourcePath = getClass()
			.getResource("/eu/dnetlib/dhp/oa/graph/dump/country/graph")
			.getPath();

		SparkFindResultsRelatedToCountry.main(new String[] {
			"-isSparkSessionManaged", Boolean.FALSE.toString(),
			"-outputPath", workingDir.toString() + "/resultWithCountry",
			"-sourcePath", sourcePath,
			"-country", "PT"
		});

		final JavaSparkContext sc = JavaSparkContext.fromSparkContext(spark.sparkContext());

		JavaRDD<String> tmp = sc
			.textFile(workingDir.toString() + "/resultWithCountry");

		Assertions.assertEquals(3, tmp.count());

		Assertions.assertTrue(tmp.collect().contains("50|06cdd3ff4700::136eb030ccb020e170df9627fa1a70af"));
		Assertions.assertTrue(tmp.collect().contains("50|06cdd3ff4700::93859bd27121c3ee7c6ee4bfb1790cbb"));
		Assertions.assertTrue(tmp.collect().contains("50|06cdd3ff4700::136eb030ccb020e170df9627fa1a70ag"));

	}
}
