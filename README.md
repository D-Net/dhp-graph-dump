# dhp-graph-dump

This project defines the oozie workflows for creating & publishing the OpenAIRE Graph dumps.

This project adheres to the Contributor Covenant [code of conduct](CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code. Please report unacceptable behavior to [dnet-team@isti.cnr.it](mailto:dnet-team@isti.cnr.it).

This project is licensed under the [AGPL v3 or later version](#LICENSE.md).