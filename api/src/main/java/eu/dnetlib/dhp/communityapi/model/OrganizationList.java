
package eu.dnetlib.dhp.communityapi.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author miriam.baglioni
 * @Date 09/10/23
 */
public class OrganizationList extends ArrayList<String> implements Serializable {

	public OrganizationList() {
		super();
	}
}
