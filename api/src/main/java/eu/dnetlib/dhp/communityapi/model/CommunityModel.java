
package eu.dnetlib.dhp.communityapi.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author miriam.baglioni
 * @Date 06/10/23
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityModel implements Serializable {
	private String id;
	private String name;
	private String description;

	private String status;

	private String type;

	private List<String> subjects;

	private String zenodoCommunity;

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public String getZenodoCommunity() {
		return zenodoCommunity;
	}

	public void setZenodoCommunity(String zenodoCommunity) {
		this.zenodoCommunity = zenodoCommunity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
