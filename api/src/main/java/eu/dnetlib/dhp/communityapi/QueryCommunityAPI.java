
package eu.dnetlib.dhp.communityapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author miriam.baglioni
 * @Date 06/10/23
 */
public class QueryCommunityAPI {
	private static final String PRODUCTION_BASE_URL = "https://services.openaire.eu/openaire/";

	private static String get(String geturl) throws IOException {
		URL url = new URL(geturl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("GET");

		int responseCode = conn.getResponseCode();
		String body = getBody(conn);
		conn.disconnect();
		if (responseCode != HttpURLConnection.HTTP_OK)
			throw new IOException("Unexpected code " + responseCode + body);

		return body;
	}

	public static String communities() throws IOException {

		return get(PRODUCTION_BASE_URL + "community/communities");
	}

	public static String community(String id) throws IOException {

		return get(PRODUCTION_BASE_URL + "community/" + id);

	}

	public static String communityDatasource(String id) throws IOException {

		return get(PRODUCTION_BASE_URL + "community/" + id + "/datasources");

	}

	public static String communityPropagationOrganization(String id) throws IOException {

		return get(PRODUCTION_BASE_URL + "community/" + id + "/propagationOrganizations");
	}

	public static String communityProjects(String id, String page, String size) throws IOException {

		return get(PRODUCTION_BASE_URL + "community/" + id + "/projects/" + page + "/" + size);
	}

	private static String getBody(HttpURLConnection conn) throws IOException {
		String body = "{}";
		try (BufferedReader br = new BufferedReader(
			new InputStreamReader(conn.getInputStream(), "utf-8"))) {
			StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}

			body = response.toString();

		}
		return body;
	}

}
