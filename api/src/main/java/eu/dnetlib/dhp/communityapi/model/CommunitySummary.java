
package eu.dnetlib.dhp.communityapi.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author miriam.baglioni
 * @Date 06/10/23
 */
public class CommunitySummary extends ArrayList<CommunityModel> implements Serializable {
	public CommunitySummary() {
		super();
	}
}
