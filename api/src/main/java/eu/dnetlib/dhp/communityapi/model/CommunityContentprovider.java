
package eu.dnetlib.dhp.communityapi.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommunityContentprovider {
	private String openaireId;

	private String enabled;

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(final String openaireId) {
		this.openaireId = openaireId;
	}

}
