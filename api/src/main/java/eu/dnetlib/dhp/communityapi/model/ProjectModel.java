
package eu.dnetlib.dhp.communityapi.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author miriam.baglioni
 * @Date 09/10/23
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectModel implements Serializable {

	private String openaireId;

	private String funder;

	private String gratId;

	public String getFunder() {
		return funder;
	}

	public void setFunder(String funder) {
		this.funder = funder;
	}

	public String getGratId() {
		return gratId;
	}

	public void setGratId(String gratId) {
		this.gratId = gratId;
	}

	public String getOpenaireId() {
		return openaireId;
	}

	public void setOpenaireId(String openaireId) {
		this.openaireId = openaireId;
	}
}
